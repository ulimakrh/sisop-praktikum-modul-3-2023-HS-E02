# Praktikum 3 Sistem Operasi 2023

Kelompok E02

1. 5025211008 - Muhammad Razan Athallah
2. 5025211232 - Ulima Kaltsum Rizky Hibatullah
3. 5025211137 - Kalyana Putri Al Kanza

## Soal 1
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut! 
(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).

a. Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.

b. Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.

c. Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.

d. Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 

e. Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.

	Catatan:
    - Pada encoding ASCII, setiap karakter diwakili oleh 8 bit atau 1 byte, yang cukup untuk merepresentasikan 256 karakter yang berbeda, contoh: 
        - Huruf A	: 01000001
        - Huruf a	: 01100001
    - Untuk karakter selain huruf tidak masuk ke perhitungan jumlah bit dan tidak perlu dihitung frekuensi kemunculannya
    - Agar lebih mudah, ubah semua huruf kecil ke huruf kapital

### Penyelesaian
Berikut adalah alur pengerjaan soal nomor 1:
- Parent process membaca file.txt dan menghitung frekuensi dari setiap huruf. Parent mengirim array berisi frekuensi kepada child.
- Child process menerima array berisi frekuensi, kemudian membuat Huffman tree dan menyimpan kode Huffman dalam suatu array. Child mengirim array berisi kode Huffman kepada parent.
- Parent menghitung perbedaan banyaknya bit antara kompresi ASCII dan kompresi Huffman, serta mengoutputkannya di console.

Pertama-tama melakukan deklarasi dan inisialisasi variabel-variabel yang akan digunakan.
```c
int fd1[2], fd2[2], count[26];
char letter[26], huffman[26][26];

for (int i = 0; i < 26; i++) letter[i] = 'A' + i, count[i] = 0;
```
Array `fd1` dan `fd2` digunakan untuk menyimpan kedua ujung dari pipe. Array `count` digunakan untuk menyimpan frekuensi tiap huruf pada file.txt. Array `letter` digunakan untuk menyimpan karakter tiap huruf (A-Z). Array `huffman` digunakan untuk menyimpan kode Huffman.  

Kemudian, dilakukan `pipe()` dan juga `fork()` untuk membagi menjadi parent process dan child process.
```c
if (pipe(fd1) == -1 || pipe(fd2) == -1) exit(1);
    
pid_t child_id = fork();
```
Selanjutnya, berikut adalah program untuk alur pertama pada parent process:
```c
else if (child_id > 0){
    close(fd1[0]);
    close(fd2[1]);

    FILE *fp = fopen("file.txt", "r");
    char c;
    while ((c = fgetc(fp)) != EOF){
        if (c >= 97 && c <= 122) count[c - 'a']++;
        else if (c >= 65 && c <= 90) count[c - 'A']++;
    }
    fclose(fp);

    write(fd1[1], count, sizeof(count));
    close(fd1[1]);

    wait(NULL);
        
    ...
}
```
Menggunakan file pointer, tiap karakter pada file.txt akan dibaca. Jika karakter tersebut merupakan huruf alfabet, maka frekuensinya akan bertambah. Setelah frekuensi dihitung, parent mengirim array frekuensi dengan fungsi `write()`.

Kemudian pada child process, akan dilakukan pembuatan tree dan penghitungan kode Huffman. Berikut adalah kode dari child:
```c
    else {
        close(fd1[1]);
        close(fd2[0]);
        read(fd1[0], count, sizeof(count));
        close(fd1[0]);

        sort(letter, count, 0, 25);
        int size = 26;
        for (int i = 0; i < 26; i++){
            if (!count[i]) size--;
            for (int j = 0; j < 26; j++) huffman[i][j] = '\0';
        }
        huffmanCodes(letter, count, huffman, size);
        
        write(fd2[1], huffman, sizeof(huffman));
        close(fd2[1]);
    }
```
Fungsi `sort()` disini melakukan merge sort pada frekuensi dan juga huruf, selain untuk mengurutkan secara ascending juga untuk memisahkan huruf dengan frekuensi 0. Kemudian perulangan for digunakan untuk menghitung huruf yang dipisahkan dan juga menginisialisasi array kode Huffman.

Selanjutnya dipanggil fungsi `huffmanCodes()` yang berisi sebagai berikut:
```c
void huffmanCodes(char letter[], int count[], char huffman[][26], int size){
    struct MinHeapNode *root = buildHuffmanTree(letter, count, size);
    int temp[26];
    printCodes(root, temp, huffman, 0);
}
```

pada fungsi tersebut akan dipanggil dua fungsi yaitu `buildHuffmanTree()` untuk membuat Huffman tree dan `printCodes()` untuk menghitung kode Huffman.

```c
struct MinHeapNode *buildHuffmanTree(char letter[], int count[], int size){
    struct MinHeapNode *left, *right, *top;
    struct MinHeap *minHeap = createAndBuildMinHeap(letter, count, size);

    while(minHeap->size != 1){
        left = getMin(minHeap);
        right = getMin(minHeap);
        top = newNode('$', left->count + right->count);
        top->left = left;
        top->right = right;
        insertMinHeap(minHeap, top);
    }

    return getMin(minHeap);
}
```
Fungsi `buildHuffmanTree()` akan terlebih dahulu membuat minimum heap sebelum membuat Huffman tree. Intinya dalam pembuatan Huffman tree, akan dilakukan dua langkah berikut secara berulang kalo:
- Dua node dengan angka terkecil akan digabung sebagai left node dan right node suatu root, root memiliki angka dari hasil penjumlahan left node dan right nodenya.
- Lakukan sorting angka tiap root secara ascending.

Langkah diatas akan diulang hingga dua node terakhir digabung menjadi satu tree saja.

```c
void printCodes(struct MinHeapNode *root, int temp[], char huffman[][26], int top){
    if (root->left){
        temp[top] = 0;
        printCodes(root->left, temp, huffman, top + 1);
    }

    if (root->right){
        temp[top] = 1;
        printCodes(root->right, temp, huffman, top + 1);
    }

    if (!(root->left) && !(root->right)){
        for (int i = 0; i < top; i++){
            if (temp[i]) huffman[root->letter - 'A'][i] = '1';
            else huffman[root->letter - 'A'][i] = '0';
        }
    }
}
```

Sedangkan pada fungsi `printCodes()` akan dilakukan traversing tree dengan menyimpan suatu array `temp`. Array tersebut akan di-append dengan angka 1 jika traverse ke right node dan angka 0 jika traverse ker left node. Ketika mencapai leaf, maka kode Huffman dari huruf pada leaf merupakan array temp tersebut.

Setelah kode Huffman disimpan pada array, maka child mengirim array tersebut ke parent. Kembali ke parent, akan dihitung perbedaan banyaknya bit dari enkripsi ASCII dan Huffman, sebagai berikut:
```c
else if (child_id > 0){
    ...

    wait(NULL);
        
    read(fd2[0], huffman, sizeof(huffman));
    printf("Huffman Codes:\n");
    for (int i = 0; i < 26; i++){
        printf("%c %s\n", 'A' + i, huffman[i]);
    }

    int before = 0, after = 0;
    fp = fopen("file.txt", "r");
    while ((c = fgetc(fp)) != EOF){
        if (c >= 97 && c <= 122) before += 8, after += strlen(huffman[c - 'a']);
        else if (c >= 65 && c <= 90) before += 8, after += strlen(huffman[c - 'A']);
    }
    fclose(fp);

    printf("\nASCII: %d bits\n", before);
    printf("Huffman: %d bits\n", after);
    close(fd2[0]);
}
```
Dideklarasikan dua variabel `before` dan `after` sebagai penhghitung banyaknya bit. Dibaca kembali file.txt, untuk setiap karakter alfabet, variabel before akan bertambah 8 (karena bit ASCII sebanyak 8) dan variabel after bertambah sebanyak panjang kode Huffman (jika kode 11001 maka bertambah 5). Terakhir perbedaan dua bit dioutputkan pada console, berikut hasilnya:

![Screenshot_2023-05-11_182150](/uploads/b75b07589606d0acf6f0c30b07afa5c6/Screenshot_2023-05-11_182150.png)

## Soal 2
Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.

a. Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

b. Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
(Catatan: wajib menerapkan konsep shared memory)

c. Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.
Contoh: 

    array [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], ...],

    maka:
    
    1 2 6 24 120 720 ... ... …

(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)

d. Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 
Dokumentasikan dan sampaikan saat demo dan laporan resmi.

### Penyelesaian
 `key` adalah sebuah bilangan kunci yang digunakan untuk mengidentifikasi shared memory. `hasil` adalah pointer ke shared memory yang akan digunakan untuk menyimpan hasil perkalian matriks. Lalu, kita mengalokasikan shared memory dengan menggunakan `shmget`. Parameter pertama adalah `key` yang digunakan untuk mengidentifikasi shared memory, parameter kedua adalah ukuran shared memory dalam byte (dalam hal ini adalah `sizeof(int[4][5])`), dan parameter terakhir adalah flag `IPC_CREAT | 0666` yang memberikan hak akses untuk shared memory. Hasil alokasi shared memory disimpan dalam variabel `shmid`. Selanjutnya, menggunakan `shmat` untuk meng-attach (menempelkan) shared memory ke dalam ruang alamat proses saat ini. Nilai `NULL` menunjukkan bahwa sistem secara otomatis menentukan alamat yang tersedia. Parameter ketiga bernilai 0 yang menunjukkan bahwa tidak ada flag khusus yang digunakan.
```c
int main(){
    key_t key = 1234;
    int (*hasil)[5];
    
    int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);
    hasil = shmat(shmid, NULL, 0);
```
Kita inisialisasi dua matriks `m1` dan `m2`. `m1` memiliki ukuran 4x2 dan `m2` memiliki ukuran 2x5. 
Kemudian, kita menggunakan fungsi `srand(time(NULL))` untuk men-generate bilangan acak dengan waktu saat ini sebagai seed. Ini dilakukan agar setiap kali program dijalankan, bilangan acak yang dihasilkan akan berbeda.
Selanjutnya, kita menggunakan dua looping `for` untuk mengisi matriks `m1` dengan bilangan acak antara 1 hingga 5. Pada setiap iterasi, nilai acak diperoleh dengan memanggil fungsi `rand()` dan mengambil modulus 5 ditambah 1. Ini berarti bilangan acak yang dihasilkan akan berada dalam rentang 1 hingga 5. Nilai ini kemudian disimpan di matriks `m1[i][j]`.
Setelah matriks `m1` terisi, kita cetak. 
```c
int m1[4][2], m2[2][5];
    srand(time(NULL));

    for(int i=0; i<4; i++){
        for(int j=0; j<2; j++){
            m1[i][j] = rand() % 5 + 1;
        }
    }

    printf("matriks 1\n");
```
Pada bagian ini, kita menggunakan dua looping `for` untuk mengisi matriks `m2` dengan bilangan acak antara 1 hingga 4. `m2` memiliki ukuran 2x5.
Pada setiap iterasi perulangan, nilai acak diperoleh dengan memanggil fungsi `rand()` dan mengambil modulus 4 ditambah 1. Ini berarti bilangan acak yang dihasilkan akan berada dalam rentang 1 hingga 4. Nilai ini kemudian disimpan di matriks `m2[i][j]`.
Setelah matriks `m2` terisi, kita mencetaknya ke layar.
```c
for(int i=0; i<4; i++){
        for(int j=0; j<2; j++){
            printf("%d ", m1[i][j]);
        }
        printf("\n");
    }

    for(int i=0; i<2; i++){
        for(int j=0; j<5; j++){
            m2[i][j] = rand() % 4 + 1;
        }
    }

    printf("\nmatriks 2\n");
```
Pada bagian ini, kita menggunakan dua looping `for` untuk mengalikan matriks `m1` dengan matriks `m2` dan menyimpan hasilnya di matriks `hasil`. `hasil` memiliki ukuran 4x5.
Pertama, sebelum melakukan perhitungan perkalian, kita menginisialisasi setiap elemen dalam matriks `hasil` menjadi 0 dengan perintah `hasil[i][j] = 0;`. Perkalian dilakukan dengan mengalikan elemen-elemen yang sesuai dari matriks `m1` dan `m2`. Variabel `k` digunakan sebagai indeks untuk mengakses elemen matriks kedua (`m2`), sedangkan indeks `i` dan `j` digunakan untuk mengakses elemen matriks hasil (`hasil`). Hasil perkalian `m1[i][k] * m2[k][j]` ditambahkan ke elemen `hasil[i][j]` dengan menggunakan operator penjumlahan (`+=`). Setelah selesai melakukan perhitungan perkalian matriks, kita mencetak matriks `hasil` ke layar dengan menggunakan dua perulangan `for` yang lain.
```c
for(int i=0; i<2; i++){
        for(int j=0; j<5; j++){
            printf("%d ", m2[i][j]);
        }
        printf("\n");
    }

    for(int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            hasil[i][j] = 0;
            for(int k=0; k<2; k++){
                hasil[i][j] += m1[i][k] * m2[k][j];
            }
        }
    }

    printf("\nmatriks hasil\n");
    for(int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }
    shmdt(hasil);

    return 0;
}
```
Lalu, untuk program cinta.c yang mengambil variabel hasil perkalian matriks dari kalian.c menggunakan thread dan multithreading, pertama kita inisialisasi fungsi `mult` yang memiliki dua parameter masukan, yaitu `c1` dan `c2`, yang merupakan dua bilangan yang akan dikalikan. Fungsi ini menghitung panjang string masing-masing bilangan dengan menggunakan fungsi `strlen()` dan menyimpannya dalam variabel `s1` dan `s2`. Fungsi kemudian membuat sebuah array `temp` dengan ukuran 300, dan mengisinya dengan angka 0 menggunakan fungsi `memset()`. Array `temp` digunakan sebagai tempat penyimpanan hasil perkalian setiap digit bilangan.
```c
char *mult(char c1[], char c2[]){
    int s1 = strlen(c1) - 1, s2 = strlen(c2) - 1, u = 0, v = 0;
    int temp[300], digits = 0;
    memset(temp, 0, sizeof(temp));
```
Fungsi juga akan melakukan perulangan dari akhir string `c1` dan `c2`, dan mengalikan setiap digit bilangan satu sama lain. Setiap hasil perkalian ditambahkan dengan elemen `temp[u+v]` yang merupakan hasil perkalian dari digit bilangan lainnya pada posisi yang sama, dan kemudian ditambahkan dengan carry dari hasil perkalian sebelumnya. Setiap kali sebuah hasil perkalian baru dihasilkan, carry akan dihitung dan disimpan untuk digunakan pada digit perkalian selanjutnya. Selain itu, variabel `v` digunakan untuk menyimpan indeks pada array `temp`. Setiap kali perulangan pada bilangan `c1` selesai, nilai dari elemen terakhir pada array `temp[u+v]` dihitung dengan carry terakhir. Kemudian, angka-angka yang berada pada array `temp` dimasukkan ke dalam variabel `res`, dengan melakukan konversi dari integer ke char menggunakan operasi `+'0'`. Akhirnya, fungsi `mult` mengembalikan `res` yang berisi hasil perkalian dari bilangan yang diberikan dalam bentuk string.
digits adalah jumlah digit hasil perkalian yang telah dihitung sebelumnya.
Dilakukan loop dari i = dig hingga i >= 0 untuk memeriksa digit-digit yang memiliki nilai 0 pada array temp.
Pada setiap iterasi loop, dilakukan pengecekan if (!temp[i]) untuk memeriksa apakah digit ke-i dari belakang adalah 0. Jika iya, maka digits dikurangi 1. Jika bukan 0, maka loop dihentikan dengan pernyataan break.
Setelah loop selesai, digits akan berisi jumlah digit yang sebenarnya dari hasil perkalian, tanpa adanya digit 0 di depan. Lalu, dilakukan alokasi memori dengan menggunakan malloc untuk menyimpan string hasil perkalian. 
Dilakukan loop dari i = 0 hingga i <= digits untuk mengisi karakter-karakter dalam string res dengan digit-digit hasil perkalian yang telah disimpan dalam array temp. Karakter digit diambil dari temp[digits - i] dengan ditambahkan karakter '0' untuk mendapatkan nilai karakter yang sesuai.
Setelah loop selesai, karakter terminasi \0 ditambahkan pada akhir string res dengan res[digits + 1] = '\0'.
```c
for (int i = s1; i >= 0; i--){
        int carry = 0, n1 = c1[i] - '0';
        v = 0;
        for (int j = s2; j >= 0; j--){
            int n2 = c2[j] - '0';
            int sum = n1 * n2 + temp[u + v] + carry;
            carry = sum / 10;
            temp[u + v] = sum % 10;
            if (u + v > digits) digits = u + v;
            v++;
        }

        temp[u + v] += carry;
        if (u + v > digits) digits = u + v;
        u++;
    }

    int dig = digits;
    for (int i = dig; i >= 0; i--){
        if (!temp[i]) digits--;
        else break;
    }

    char *res = (char*) malloc((digits + 1) * sizeof(char));
    for (int i = 0; i <= digits; i++) res[i] = temp[digits - i] + '0';
    res[digits + 1] = '\0';
    return res;
}
```
Fungsi `hitung_faktorial` digunakan untuk menghitung faktorial dari suatu angka. Fungsi ini menggunakan thread dalam pemrosesannya. Selanjutnya, fungsi ini membuat dua array char, yaitu `res` dan `c`, dengan ukuran 200 untuk menyimpan hasil faktorial dan bilangan dalam bentuk string. Array `res` diinisialisasi dengan string "1", karena faktorial dari 0 dan 1 adalah 1. Kemudian, fungsi melakukan perulangan dari `num` hingga 1 untuk menghitung faktorial. Pada setiap iterasi, bilangan `i` diubah menjadi string menggunakan fungsi `sprintf()` dan disimpan dalam array `c`. Kemudian, fungsi `mult()` dipanggil dengan argumen `res` (hasil faktorial sebelumnya) dan `c` (bilangan saat ini) untuk mengalikan dan memperbarui hasil faktorial dalam array `res`. Ini dilakukan dengan meng-copy hasil perkalian ke dalam array `res` menggunakan fungsi `strcpy()`. Setelah selesai melakukan perulangan, hasil faktorial terakhir disimpan dalam array `res`. Kemudian, hasil faktorial tersebut dicetak ke layar menggunakan fungsi `printf()`.
```c
void *hitung_faktorial(void *args){
    int num = *((int *)args);

    char res[200], c[200];
    strcpy(res, "1");
    for (int i = num; i >= 1; i--){
        sprintf(c, "%d", i);
        strcpy(res, mult(res, c));
    }

    printf("%s ", res);
}
```
Lalu di fungsi `main` sendiri menggunakan beberapa fitur lainnya, seperti shared memory dan thread, untuk menghitung faktorial dari matriks hasil perkalian. Di awal fungsi `main`, kita menggunakan `clock_t` untuk mengukur waktu eksekusi. Variabel `start` diinisialisasi dengan waktu saat ini menggunakan fungsi `clock()`. Selanjutnya, kita mendefinisikan variabel `key` yang digunakan untuk mengakses shared memory. Kemudian, kita mendeklarasikan variabel pointer `hasil` yang akan digunakan untuk menunjuk ke shared memory yang berisi hasil perkalian matriks. Kita menggunakan fungsi `shmget()` untuk mendapatkan ID shared memory berdasarkan `key` dan ukuran matriks hasil perkalian (`sizeof(int[4][5])`). Mode akses shared memory adalah 0666 (hak akses untuk membaca dan menulis). Selanjutnya, kita menggunakan fungsi `shmat()` untuk menautkan shared memory ke variabel `hasil`. Nilai `shmid` (ID shared memory) digunakan sebagai argumen pertama, sedangkan argumen kedua dan ketiga diisi dengan `NULL` dan `0` untuk memberikan nilai default. Kemudian, kita mencetak matriks hasil perkalian ke layar menggunakan perulangan `for`. Setiap elemen dari matriks hasil dicetak menggunakan perintah `printf("%d ", hasil[i][j])`. Setelah mencetak semua elemen dalam satu baris, kita mencetak karakter baru \n untuk pindah ke baris berikutnya. Kode yang diberikan mencetak matriks hasil perkalian pada layar. Berikut adalah penjelasan detail mengenai kode tersebut: Loop `for` pertama digunakan untuk mengiterasi baris matriks. Loop ini berjalan dari 0 hingga 3 karena terdapat 4 baris dalam matriks. Loop kedua digunakan untuk mengiterasi kolom matriks. Loop ini berjalan dari 0 hingga 4 karena terdapat 5 kolom dalam matriks. Pada setiap iterasi loop kedua, elemen matriks pada baris `i` dan kolom `j` dicetak menggunakan `printf("%d ", hasil[i][j])`. `%d` digunakan untuk mencetak nilai integer, dan `hasil[i][j]` adalah nilai elemen matriks pada posisi tersebut.

Setelah itu, kita mencetak "matriks hasil faktorial" dan menggunakan loop `for` untuk mengiterasi setiap elemen dalam matriks hasil. Pada setiap iterasi, kita mengalokasikan memori untuk menyimpan nilai elemen matriks hasil dalam variabel `num`. Kemudian, kita mengisi `num` dengan nilai elemen matriks hasil `hasil[i][j]` dan melewatkan `num` sebagai argumen ke fungsi `pthread_create()`. Ini akan membuat thread baru yang menjalankan fungsi `hitung_faktorial` untuk menghitung faktorial dari `num`. Kemudian, kita memanggil `pthread_join()` untuk menunggu thread selesai sebelum melanjutkan ke iterasi berikutnya. Setelah selesai, kita mencetak karakter baru (`\n`) untuk memisahkan baris matriks faktorial setiap kali loop iterasi di bagian dalam perulangan selesai.
Setelah selesai melakukan perulangan, kita menggunakan `shmdt()` untuk melepaskan shared memory dari variabel `hasil`.

Selanjutnya, kita menghitung waktu eksekusi dengan memperoleh waktu saat ini menggunakan `clock()` dan menyimpannya di variabel `end`. Kemudian, kita menghitung durasi waktu yang dihabiskan dalam eksekusi dengan membagi selisih waktu antara `end` dan `start` dengan `CLOCKS_PER_SEC`. Hasilnya disimpan dalam variabel `time`. Terakhir, kita mencetak waktu eksekusi dengan menggunakan `printf("\ntime taken: %lf\n", time)`.
```c
int main(){
    clock_t start, end;
    start = clock();
    key_t key = 1234;
    int (*hasil)[5];
    
    int shmid = shmget(key, sizeof(int[4][5]), 0666);
    hasil = shmat(shmid, NULL, 0);

    printf("matriks hasil perkalian\n");
    for(int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }

    printf("\nmatriks hasil faktorial\n");
    pthread_t t_id[20];
    int index = 0;
    for(int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            int *num = malloc(sizeof(int[4][5]));
            *num = hasil[i][j];
            pthread_create(&t_id[index], NULL, &hitung_faktorial, num);
            pthread_join(t_id[index], NULL);
            index++;
        }
        printf("\n");
    }
    
    shmdt(hasil);
    end = clock();
    double time = (double)(end - start) / CLOCKS_PER_SEC;;
    printf("\ntime taken: %lf\n", time);
    return 0;
}
```
##### Untuk program sisop.c sendiri pada dasarnya sama dengan cinta.c. Letak perbedaannya adalah sisop.c tidak menggunakan thread dalam programnya. Perbedaan performa di antara keduanya adalah cinta.c membutuhkan waktu proses lebih lama ketimbang sisop.c.

![Screenshot_2023-05-13_142541](/uploads/d0b4766829a99a2946cd06587dab8d55/Screenshot_2023-05-13_142541.png)

![Screenshot_2023-05-13_142552](/uploads/f4971d31db3f5f8b76c383c24867b34b/Screenshot_2023-05-13_142552.png)

![Screenshot_2023-05-13_142601](/uploads/b3fe7634f625be059ac04693e4fa23d6/Screenshot_2023-05-13_142601.png)


## Soal 3
Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun, Elshe tidak paham harus mulai dari mana.

a. Bantulah Elshe untuk membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.

b. User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json (dapat diunduh manual saja melalui link berikut) sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.
Proses decrypt dilakukan oleh program stream.c tanpa menggunakan koneksi socket sehingga struktur direktorinya adalah sebagai berikut:

    └── soal3
        ├── playlist.txt
        ├── song-playlist.json
        ├── stream.c
        └── user.c

c. Selain itu, user dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt

Sample Output:

    17 - MK
    1-800-273-8255 - Logic
    1950 - King Princess
    …
    Your Love Is My Drug - Kesha
    YOUTH - Troye Sivan
    ZEZE (feat. Travis Scott & Offset) - Kodak Black

d. User juga dapat mengirimkan perintah PLAY <SONG> dengan ketentuan sebagai berikut.

    PLAY "Stereo Heart" 
     sistem akan menampilkan: 
        USER <USER_ID> PLAYING "GYM CLASS HEROES - STEREO HEART"
    PLAY "BREAK"
     sistem akan menampilkan:
        THERE ARE "N" SONG CONTAINING "BREAK":
        1. THE SCRIPT - BREAKEVEN
        2. ARIANA GRANDE - BREAK FREE
    dengan “N” merupakan banyaknya lagu yang sesuai dengan string query. Untuk contoh di atas berarti THERE ARE "2" SONG CONTAINING "BREAK":
    PLAY "UVUWEVWEVWVE"
        THERE IS NO SONG CONTAINING "UVUVWEVWEVWE"

Untuk mempermudah dan memperpendek kodingan, query bersifat tidak case sensitive 😀

e. User juga dapat menambahkan lagu ke dalam playlist dengan syarat sebagai berikut:
1. User mengirimkan perintah
        
        ADD <SONG1>
        ADD <SONG2>
        sistem akan menampilkan:
        USER <ID_USER> ADD <SONG1>

2. User dapat mengedit playlist secara bersamaan tetapi lagu yang ditambahkan tidak boleh sama. Apabila terdapat lagu yang sama maka sistem akan meng-output-kan “SONG ALREADY ON PLAYLIST”

f. Karena Elshe hanya memiliki resource yang kecil, untuk saat ini Elshe hanya dapat memiliki dua user. Gunakan semaphore (wajib) untuk membatasi user yang mengakses playlist.
Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga mengirim perintah apapun.
g. Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".

Catatan: 
- Untuk mengerjakan soal ini dapat menggunakan contoh implementasi message queue pada modul.
- Perintah DECRYPT akan melakukan decrypt/decode/konversi dengan metode sebagai berikut:
1. ROT13

ROT13 atau rotate 13 merupakan metode enkripsi sederhana untuk melakukan enkripsi pada tiap karakter di string dengan menggesernya sebanyak 13 karakter.

2. Base64

Base64 adalah sistem encoding dari data biner menjadi teks yang menjamin tidak terjadinya modifikasi yang akan merubah datanya selama proses transportasi. Tools Decode/Encode.

3. Hex

Hexadecimal string merupakan kombinasi bilangan hexadesimal (0-9 dan A-F) yang merepresentasikan suatu string. Tools.

### Penyelesaian
Pertama-tama, variabel user1Id, user2Id, dan usrnum dideklarasikan sebagai variabel integer dan diberi nilai awal 1. Struct "user" dideklarasikan dengan digunakan untuk menyimpan informasi tentang pengguna (user). "id" adalah identitas dari pengguna, dan "command" adalah perintah (command) yang diberikan oleh pengguna.
```c
int user1Id = 1, user2Id = 1, usrnum = 1;

struct user {
	int id;
	char command[1024];
};
```
Fungsi `msgidGenerator()` menghasilkan message queue identifier dengan memanggil sistem `ftok()` dan `msgget()`. Identifier ini dapat digunakan untuk membuat atau mengakses antrian pesan. Looping for akan menghasilkan uniquekey untuk message queue menggunakan `ftok()`. Argumen pertama ada nama file yang digunakan (dalam case ini adalah user). Jika `ftok()` gagal maka akan return -1 dan mengeset errno untuk mengindikasikan error.
```c
int msgidGenerator() {
    key_t key;
    
    if((key = ftok("user", 65)) == -1) {
        perror("ftok");
        return -1;
    }
```
Fungsi `msgid` akan membuat atau mengakses message queue menggunakan generated key and `msgget()`. Argumen pertama adalah key generated menggunakan `ftok()`. Argumen kedua adalah satu set flags yang mengontrol pembuatan dan akses message queue. Flags akan membuat message queue apabila belum ada sebelumnya, dan harus bisa dibaca dan ditulis oleh semua user. Jika fungsi `msget()` gagal maka akan return -1 dan mengeset errno untuk mengindikasikan error.
```c
int msgid;
    if((msgid = msgget(key, 0666 | IPC_CREAT)) == -1) {
        perror("msgid");
        return -1;
    }

    // Return the message queue identifier.
    return msgid;
}
```
Fungsi `add` menerima argumen berupa pointer ke sebuah array karakter `word` dan sebuah bilangan bulat `id`. Fungsi ini bertugas untuk memeriksa apakah lagu yang diberikan `word` sudah ada dalam file "playlist.txt". Pointer file `fp` dideklarasikan dan membuka file "playlist.txt" dalam mode baca menggunakan fungsi `fopen`. Kemudian, array karakter `line` dideklarasikan untuk menyimpan setiap baris dari file. Loop `while` akan membaca setiap baris dari file menggunakan fungsi `fgets`dan akan terus berjalan hingga fungsi `fgets` mengembalikan `NULL`, yang menandakan tidak ada baris lagi yang dapat dibaca. Di dalam loop, kode memeriksa apakah baris saat ini yang dibaca dari file `line` sama dengan lagu yang ingin ditambahkan `word` menggunakan fungsi `strcmp` dengan membandingkan dua string dan mengembalikan 0 jika kedua string tersebut sama. Jika baris saat ini sama dengan lagu yang ingin ditambahkan, artinya lagu tersebut sudah ada dalam playlist. Dalam hal ini, kode akan mencetak pesan "LAGU SUDAH ADA DALAM PLAYLIST", yang menandakan bahwa lagu tersebut sudah ada dalam playlist. Setelah mencetak pesan, kode menutup file menggunakan fungsi `fclose` dan keluar dari fungsi tanpa melakukan hal lain. Ini berarti fungsi berakhir tanpa menambahkan lagu ke dalam playlist karena lagu tersebut sudah ada sebelumnya.
```c
    // Declare a file pointer and open the "playlist.txt" file in read mode
    FILE *fp = fopen("playlist.txt", "r");    

    // Declare a character array to hold each line of the file
    char line[100];    

     // Use a loop to read each line of the file
    while (fgets(line, 100, fp) != NULL) {   

        // Check if the current line is the same as the word to be added
        if (strcmp(line, word) == 0) {    

            // If it is, print a message indicating that the song is already on the playlist
            printf("SONG ALREADY ON PLAYLIST\n");    

            // Close the file
            fclose(fp);    

            // Return from the function without doing anything else
            return;    
        }
    }
```
Fungsi `play` bertujuan untuk mencari lagu dalam file "playlist.txt" berdasarkan kata kunci yang diberikan. File "playlist.txt" dibuka menggunakan fungsi `fopen` dengan mode "r" untuk membaca file. Jika file tidak dapat dibuka, maka akan dicetak pesan kesalahan "Failed to open playlist.txt". Array karakter `line` dideklarasikan untuk menyimpan setiap baris yang dibaca dari file. Array dua dimensi `lst` untuk menyimpan lagu-lagu yang cocok dengan kata kunci. Variabel `found_count` sebagai penghitung jumlah lagu yang ditemukan yang awalnya diinisialisasi dengan nilai 0. Array karakter `search_word` untuk menyimpan kata kunci yang dicari dalam bentuk huruf kecil. Lalu, dilakukan pengulangan `for` untuk mengubah setiap karakter dalam kata kunci menjadi huruf kecil menggunakan fungsi `tolower`. Hal ini dilakukan untuk memastikan pencarian yang tidak bersifat case-sensitive. Hasil konversi disimpan dalam array `search_word`. Juga dihitung panjang kata kunci dengan mengiterasi sampai karakter null-terminating \0 ditemukan. 
Loop `while` akan membaca file baris per baris menggunakan fungsi `fgets` dan setiap baris yang dibaca disimpan dalam array `line`. Di dalam loop, panjang baris saat ini dihitung menggunakan fungsi `strlen` dan disimpan dalam variabel `line_length`. Array karakter `lower_line` dideklarasikan untuk menyimpan baris dalam bentuk huruf kecil. Lalu, dicek apakah kata kunci yang dicari terdapat dalam baris menggunakan fungsi `strstr`. Jika kata kunci ditemukan, maka baris tersebut merupakan lagu yang cocok dengan kata kunci. Baris tersebut disalin ke dalam array `lst` menggunakan fungsi `sprintf`, dan variabel `found_count` diincrement untuk menghitung jumlah lagu yang ditemukan. Loop akan terus berlanjut hingga semua baris dalam file telah dibaca.
```c
void play(char* word, int id) {
    FILE* file = fopen("playlist.txt", "r"); // Open the playlist file
    if (!file) {
        printf("Failed to open playlist.txt\n"); // If the file can't be opened, print an error message and return
        return;
    }

    char line[1000];
    char lst[1000][1000]; // Create an array to store matching songs
    int found_count = 0; // Counter for the number of matches found

    char search_word[1000];
    int i;
    for (i = 0; word[i]; i++) { // Convert the search term to lowercase
        search_word[i] = tolower(word[i]);
    }
    search_word[i] = '\0';

    while (fgets(line, 1000, file)) { // Read the file line by line
        int line_length = strlen(line);

        char lower_line[1000];
        int j;
        for (j = 0; j < line_length; j++) { // Convert the line to lowercase
            lower_line[j] = tolower(line[j]);
        }
        lower_line[j] = '\0';

        if (strstr(lower_line, search_word) != NULL) { // Check if the search term is in the line
            sprintf(lst[found_count], "%s", line); // Add the line to the list of matches
            found_count++; // Increment the counter
        }
    }
```
Lalu, dilakukan pengecekan apakah jumlah lagu yang ditemukan `found_count` adalah 1. Jika demikian, berarti hanya ada satu lagu yang cocok dengan kata kunci yang dicari. Dalam hal ini, kode mencetak pesan "USER <id> PLAYING "<nama lagu>"", di mana `<id>` adalah parameter yang diberikan ke fungsi dan `<nama lagu>` adalah lagu yang ditemukan dan disimpan dalam array `lst` dengan indeks 0.
Jika `found_count` bernilai 0, berarti tidak ada lagu yang cocok dengan kata kunci yang dicari. Dalam hal ini, kode mencetak pesan "THERE IS NO SONG CONTAINING "<kata kunci>"" di mana `<kata kunci>` adalah kata kunci yang diberikan sebagai parameter ke fungsi.
Jika `found_count` > 1, berarti terdapat beberapa lagu yang cocok dengan kata kunci yang dicari. Dalam hal ini, kode mencetak pesan "THERE ARE <jumlah lagu> SONG CONTAINING "<kata kunci>"" di mana `<jumlah lagu>` adalah nilai dari `found_count`, dan `<kata kunci>` adalah kata kunci yang diberikan sebagai parameter ke fungsi.
Setelah itu, dilakukan loop `for` untuk mencetak daftar lagu yang cocok. Setiap lagu dicetak dengan format "<nomor>. <nama lagu>". Nomor lagu dihitung dengan `i+1` karena indeks array dimulai dari 0.
Setelah semua pesan atau daftar lagu dicetak, file "playlist.txt" ditutup menggunakan fungsi `fclose` untuk membersihkan sumber daya yang digunakan.
```c
 if(found_count == 1) { // If there's only one match, play the song
        printf("USER <%d> PLAYING \"%s\"\n", id, lst[0]);
    }
    else if (found_count == 0) { // If there are no matches, print an error message
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", word);
    }
    else { // If there are multiple matches, print a list of them
        printf("THERE ARE %d SONG CONTAINING \"%s\"\n", found_count, word);
        for (int i=0; i<found_count; i++) {
            printf("%d. %s", i+1, lst[i]);
        }
    }

    fclose(file); // Close the file
}
```
Fungsi bernama `list()`bertujuan untuk membuka file dengan nama "playlist.txt" untuk dibaca dan mencetak isinya ke layar. Variabel `filePointer` dideklarasikan dengan tipe `FILE*`yaitu tipe data yang digunakan untuk menunjukkan pointer ke file.
Lalu, diperiksa apakah file berhasil dibuka atau tidak dengan melakukan pengecekan apakah `filePointer`=`NULL`. Jika iya, maka artinya file gagal dibuka. Dalam hal ini, kode akan mencetak pesan "File cannot be opened.". Jika file berhasil dibuka, maka kode akan melanjutkan ke instruksi selanjutnya untuk membaca dan mencetak isi file. Lalu, variabel `ch` akan digunakan untuk menyimpan karakter yang dibaca dari file. Loop while akan berjalan selama karakter yang dibaca dari file menggunakan fungsi fgetc() bukanlah EOF (end-of-file). Di dalam loop, karakter yang dibaca dari file disimpan dalam variabel ch. Kemudian, karakter tersebut dicetak ke layar. Setelah selesai membaca dan mencetak isi file, kode akan menutup file menggunakan fungsi `fclose()`.
```c
char ch;
   // Read characters from file until EOF is reached
   while ((ch = fgetc(filePointer)) != EOF)
      printf("%c", ch); // Print each character

   fclose(filePointer); // Close the file
```
Fungsi `rot13` mengimplementasikan algoritma ROT13 (rotate by 13 places) untuk melakukan enkripsi teks. Fungsi ini menerima dua parameter: `FILE *file` yang merupakan pointer ke file yang akan ditulis, dan `char *str` yang merupakan string yang akan dienkripsi menggunakan algoritma ROT13. Pertama, fungsi `rot13` menerima pointer ke file yang akan ditulis (`FILE *file`) dan string yang akan dienkripsi (`char *str`). Loop `for` mengiterasi melalui setiap karakter dalam string `str` dan akan berjalan sampai karakter saat ini (`str[i]`) adalah karakter null \0 yang menandakan akhir dari string. Di dalam loop, dilakukan pengecekan pada karakter saat ini (`str[i]`) untuk menentukan tindakan yang harus dilakukan. 
- Jika karakter saat ini adalah huruf kecil (range 'a' - 'm') atau huruf besar (range 'A' - 'M'), maka karakter tersebut akan dienkripsi dengan menambahkan 13 ke nilai ASCII-nya.
- Jika karakter saat ini adalah huruf kecil (range 'n' - 'z') atau huruf besar (range 'N' - 'Z'), maka karakter tersebut akan dienkripsi dengan mengurangi 13 dari nilai ASCII-nya. Hal ini dilakukan untuk membalikkan enkripsi ROT13. Hasil enkripsi kemudian ditulis ke file menggunakan `fputc()`.
- Jika karakter saat ini bukan huruf (baik huruf kecil maupun huruf besar), maka karakter tersebut akan ditulis ke file tanpa modifikasi menggunakan `fputc()`.

Setelah loop selesai, enkripsi ROT13 selesai dan semua karakter yang dienkripsi telah ditulis ke file yang ditunjuk oleh `file`.
```c
void rot13(FILE *file, char *str) {
    // Loop through each character in the string
    for (int i=0; str[i]!='\0'; i++) {
        if ((str[i] >= 'a' && str[i] <= 'm') || (str[i] >= 'A' && str[i] <= 'M')) {
            fputc((str[i] + 13), file); // If the character is in range A-M or a-m, add 13 to its ASCII value and write to file
        } else if ((str[i] >= 'n' && str[i] <= 'z') || (str[i] >= 'N' && str[i] <= 'Z')) {
            fputc((str[i] - 13), file); // If the character is in range N-Z or n-z, subtract 13 from its ASCII value and write to file
        }
        else {
            fputc(str[i], file); // If the character is not a letter, write it to file without any modification
        }
    }
}
```
Fungsi `base64` mengimplementasikan enkripsi base64 pada sebuah input dan menerima tiga parameter: `char *input` yang merupakan input yang akan dienkripsi, `size_t input_len` yang merupakan panjang input, dan `size_t *output_len` yang akan menyimpan panjang output yang dihasilkan. Di dalam fungsi, beberapa variabel dideklarasikan, yaitu `bio` dan `b64` dengan tipe `BIO*`. `BIO` merupakan struktur yang digunakan dalam OpenSSL untuk membaca atau menulis data dalam berbagai format. Objek `bio` dibuat dengan memori sebagai sumber datanya menggunakan `BIO_new_mem_buf()`. Fungsi ini digunakan untuk membuat objek `BIO` yang akan membaca data dari buffer memori. Objek `b64` dibuat dengan tipe `BIO_f_base64()` yang akan melakukan enkripsi base64 pada data dan dihubungkan ke `bio` menggunakan `BIO_push()`. Hal ini dilakukan untuk menggabungkan objek `b64` sebagai filter atau transformasi data pada objek `bio`. Flag `BIO_FLAGS_BASE64_NO_NL` diatur pada objek `bio` menggunakan `BIO_set_flags()` dan akan menunjukkan bahwa output base64 tidak akan mencakup karakter newline ('\n'). Data dari objek `bio` dibaca dan disimpan ke dalam output menggunakan `BIO_read()`. Hasil pembacaan akan disimpan dalam `output` dan panjang output akan disimpan dalam `*output_len`. Lalu, return output yang telah dienkripsi.
```c
char *base64(char *input, size_t input_len, size_t *output_len) {
    // Declare variables
    BIO *bio, *b64;
    char *output = (char*)malloc(input_len); // Allocate memory for output
    memset(output, 0, input_len); // Initialize output to zero
    
    // Create BIO objects
    bio = BIO_new_mem_buf(input, input_len); 
    b64 = BIO_new(BIO_f_base64()); 
    bio = BIO_push(b64, bio); 
    
    // Set flags for BIO
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

    // Read from BIO and store in output
    *output_len = BIO_read(bio, output, input_len); 
    
    // Free memory and return output
    BIO_free_all(bio); 
    return output;
}
```
Fungsi `hex` mengimplementasikan konversi dari string heksadesimal menjadi data byte dan menulisnya ke file yang ditentukan. Fungsi ini menerima dua parameter: `FILE *file` yang merupakan pointer ke file yang akan ditulis, dan `char *str` yang merupakan string heksadesimal yang akan dikonversi. Fungsi menggunakan loop `while` untuk melakukan iterasi selama karakter saat ini (`*str`) bukan karakter null \0 yang menandakan akhir dari string. Di dalam loop, terdapat loop `for` yang akan membaca dua karakter sekaligus dari string heksadesimal (`*str`).Lalu, loop `for` akan berjalan maksimal dua kali dan hanya jika karakter saat ini (`*str`) tidak null. Di dalam loop `for`, dilakukan penggeseran nilai sebelumnya (`value`) ke kiri sejauh 4 bit menggunakan operator `<<= 4`. Hal ini dilakukan untuk memberikan ruang pada 4 bit terakhir untuk menampung nilai heksadesimal yang akan ditambahkan. Dilakukan pengecekan pada karakter saat ini (`*str`) untuk menentukan nilai heksadesimal yang akan ditambahkan ke `value`.
   - Jika karakter saat ini adalah angka dari '0' hingga '9', maka nilai heksadesimalnya dikonversi dari ASCII ke integer dengan cara mengurangi nilai karakter '0' dan kemudian di-OR-kan dengan `value`.
   - Jika karakter saat ini adalah huruf kecil dari 'a' hingga 'f', maka nilai heksadesimalnya dikonversi dari ASCII ke integer dengan cara mengurangi nilai karakter 'a' dan ditambahkan dengan 10, kemudian di-OR-kan dengan `value`.
   - Jika karakter saat ini adalah huruf besar dari 'A' hingga 'F', maka nilai heksadesimalnya dikonversi dari ASCII ke integer dengan cara mengurangi nilai karakter 'A' dan ditambahkan dengan 10, kemudian di-OR-kan dengan `value`.
   - Jika karakter saat ini tidak valid (bukan angka atau huruf heksadesimal), maka pesan error akan dicetak ke `stderr` menggunakan `fprintf()` dan fungsi akan mengembalikan kontrol dengan pernyataan `return`.
Setelah mengolah dua karakter heksadesimal, pointer `str` diinkremenkan agar menunjuk ke dua karakter heksadesimal berikutnya dalam string. Nilai `value` yang telah dikonversi dari heksadesimal ke integer akan ditulis ke file menggunakan `fputc()`. Setelah loop selesai, konversi heksadesimal selesai dan data byte hasil konversi telah ditulis ke file yang ditunjuk oleh `file`.
```c
void hex(FILE *file, char *str) {
    while (*str) {
        int value = 0;
        for (int i = 0; i < 2 && *str; i++) { // Read two characters at a time
            value <<= 4; // Shift previous value left by 4 bits
            if (*str >= '0' && *str <= '9') { // Convert from ASCII to integer
                value |= (*str - '0');
            } else if (*str >= 'a' && *str <= 'f') {
                value |= (*str - 'a' + 10);
            } else if (*str >= 'A' && *str <= 'F') {
                value |= (*str - 'A' + 10);
            } else {
                fprintf(stderr, "Error: Invalid hexadecimal digit '%c'\n", *str);
                return; // Stop processing if invalid digit is encountered
            }
            str++;
        }
        fputc(value, file); // Write value to file
    }
}
```
Fungsi `swap` mengambil dua pointer ke pointer char dan menukar nilai mereka.
```c
void swap(char **a, char **b) {
    char *temp = *a;
    *a = *b;
    *b = temp;
}
```
Fungsi `partition` mengimplementasikan algoritma partisi dalam algoritma quicksort untuk mengurutkan array of strings (`char **lines` ini menerima tiga parameter: `lines` yang merupakan array of strings, `left` yang merupakan indeks kiri dari rentang yang akan dipartisi, dan `right` yang merupakan indeks kanan dari rentang yang akan dipartisi.Variabel `pivot` diinisialisasi dengan nilai yang ditunjuk oleh `lines[right]`yang digunakan sebagai pivot atau titik referensi untuk mempartisi array. Variabel `i` diinisialisasi dengan `left - 1`dan akan menjadi indeks untuk elemen terakhir yang lebih kecil dari pivot saat mempartisi array.
Loop `for` mengiterasi melalui indeks `j` mulai dari `left` hingga `right-1`, digunakan untuk membandingkan setiap elemen dengan pivot. Di dalam loop, dilakukan pengecekan dengan menggunakan fungsi `strcasecmp()` untuk membandingkan string pada `lines[j]` dengan pivot. Jika string pada `lines[j]` lebih kecil daripada pivot, maka:
   - Nilai `i` diinkremenkan (`i++`) untuk menunjukkan bahwa ada elemen lebih kecil dari pivot yang telah ditemukan.
   - Dilakukan pemanggilan fungsi `swap()` untuk menukar nilai yang ditunjuk oleh `lines[i]` dengan nilai yang ditunjuk oleh `lines[j]`. Fungsi `swap()` digunakan untuk menukar dua elemen dalam array of strings.

Setelah loop selesai, semua elemen telah dibandingkan dengan pivot dan dipartisi. Elemen-elemen yang lebih kecil dari pivot ditempatkan pada posisi sebelum `i`, sementara elemen-elemen yang lebih besar atau sama dengan pivot tetap pada posisi setelah `i`.Lalu, dilakukan pemanggilan fungsi `swap()` untuk menukar nilai yang ditunjuk oleh `lines[i + 1]` dengan nilai yang ditunjuk oleh `lines[right]`. Ini mengembalikan pivot ke posisinya yang tepat dalam array setelah selesai mempartisi. Fungsi mengembalikan nilai `i + 1` yang merupakan indeks pivot yang baru.
```c
int partition(char **lines, int left, int right) {
    char *pivot = lines[right];
    int i = left - 1;
    for (int j = left; j < right; j++) {
        if (strcasecmp(lines[j], pivot) < 0) {
            i++;
            swap(&lines[i], &lines[j]);
        }
    }
    swap(&lines[i + 1], &lines[right]);
    return i + 1;
}
```
Kode tersebut adalah implementasi algoritma quicksort untuk mengurutkan kumpulan string yang diwakili sebagai array of char pointers (`char **lines`). Fungsi `quicksort` menerima tiga parameter: array `lines`, indeks `left`, dan indeks `right`. Indeks `left` menunjukkan batas kiri dari array, sedangkan indeks `right` menunjukkan batas kanan dari array. Pertama-tama, fungsi `quicksort` memeriksa apakah array yang akan diurutkan memiliki lebih dari satu elemen. Jika array hanya memiliki satu elemen atau kurang, maka array dianggap sudah terurut dan fungsi `quicksort` tidak melakukan apa-apa. Jika array memiliki lebih dari satu elemen, maka fungsi `partition` dipanggil untuk memilih elemen pivot dan membagi array menjadi dua bagian. Kemudian, fungsi `quicksort` dipanggil secara rekursif untuk mengurutkan setiap bagian array yang lebih kecil dari pivot secara terpisah. Akhirnya, elemen pivot ditempatkan pada posisi yang benar di antara dua bagian array yang terurut.
```c
void quicksort(char **lines, int left, int right) {
    if (left < right) {
        int pivot_idx = partition(lines, left, right);
        quicksort(lines, left, pivot_idx - 1);
        quicksort(lines, pivot_idx + 1, right);
    }
}
```
Fungsi `sort` mengimplementasikan proses pengurutan isi file "playlist.txt" menggunakan algoritma quicksort.  Di awal fungsi, variabel `MAX_LINE_LEN` diinisialisasi dengan nilai 10000. Ini adalah panjang maksimum yang diizinkan untuk setiap baris dalam file. File "playlist.txt" dibuka dengan mode pembacaan menggunakan `fopen()`. Jika file tidak dapat dibuka, pesan error akan dicetak dan fungsi akan berhenti dengan pernyataan `return`. Dilakukan alokasi memori dinamis untuk array `lines` dengan ukuran `MAX_LINE_LEN` menggunakan `malloc()`. Array ini akan digunakan untuk menyimpan setiap baris yang dibaca dari file. Variabel `num_lines` diinisialisasi dengan nilai 0. Ini akan digunakan untuk menghitung jumlah baris yang telah dibaca dari file. Dalam loop `while`, dilakukan pembacaan baris dari file menggunakan `fgets()`. Setiap baris yang berhasil dibaca disalin ke dalam elemen yang sesuai di array `lines` menggunakan `strdup()`, dan `num_lines` diinkremenkan. Setelah semua baris dibaca, file "playlist.txt" ditutup menggunakan `fclose()`. Dilakukan pemanggilan fungsi `quicksort()` untuk mengurutkan array `lines` secara rekursif dari indeks 0 hingga `num_lines - 1`. File "playlist.txt" dibuka kembali dengan mode penulisan menggunakan `fopen()`. Dilakukan loop `for` untuk menulis setiap baris yang telah diurutkan ke dalam file menggunakan `fprintf()`. Setiap elemen dalam array `lines` ditulis sebagai baris pada file, dan memori yang dialokasikan untuk setiap elemen di dalam array dihapus menggunakan `free()`. Setelah semua baris ditulis, file "playlist.txt" ditutup kembali menggunakan `fclose()`. Memori yang dialokasikan untuk array `lines` juga dihapus menggunakan `free()`. Pesan "File has been sorted." dicetak ke konsol sebagai konfirmasi bahwa proses pengurutan selesai.
```c
void sort() {
    int MAX_LINE_LEN = 10000;
    FILE *fp = fopen("playlist.txt", "r");
    if (!fp) {
        printf("Error: cannot open file playlist.txt\n");
        return ;
    }

    char **lines = malloc(sizeof(char*) * MAX_LINE_LEN);
    int num_lines = 0;
    char line[MAX_LINE_LEN];
    while (fgets(line, MAX_LINE_LEN, fp) != NULL) {
        lines[num_lines] = strdup(line);
        num_lines++;
    }
    fclose(fp);

    quicksort(lines, 0, num_lines - 1);

    fp = fopen("playlist.txt", "w");
    for (int i = 0; i < num_lines; i++) {
        fprintf(fp, "%s", lines[i]);
        free(lines[i]);
    }
    fclose(fp);
    free(lines);

    printf("File has been sorted.\n");

}
```
Fungsi `decrypt` melakukan dekripsi terhadap data lagu dalam file "song-playlist.json" dan menuliskan hasil dekripsi ke file "playlist.txt". Di awal fungsi, file "song-playlist.json" dibuka untuk dibaca (`"r"`) dan file "playlist.txt" dibuka untuk ditulis (`"w"`) menggunakan `fopen()`. Dilakukan pengecekan apakah kedua file berhasil dibuka. Jika salah satu file gagal dibuka, pesan error akan dicetak dan fungsi akan berhenti dengan pernyataan `return`. Dilakukan parsing objek JSON dari file input menggunakan `json_object_from_file()`. Hasil parsing disimpan dalam variabel `jsonObj`. Dilakukan pengecekan apakah parsing JSON berhasil. Jika parsing gagal, pesan error akan dicetak, kedua file akan ditutup menggunakan `fclose()`, dan fungsi akan berhenti dengan pernyataan `return`. Dideklarasikan variabel `jsonMethod` dan `jsonSong` untuk menyimpan nilai dari properti JSON "method" dan "song", serta variabel `method` dan `song` sebagai representasi string dalam bahasa C. Dihitung panjang array JSON menggunakan `json_object_array_length()`, dan hasilnya disimpan dalam variabel `len`. Dilakukan looping untuk setiap objek dalam array JSON menggunakan `for` dengan variabel `i` sebagai indeks. Pada setiap iterasi, dilakukan pengambilan objek JSON pada indeks yang sedang ditunjuk menggunakan `json_object_array_get_idx()`, dan hasilnya disimpan dalam variabel `jsonIndex`. Dilakukan pengambilan nilai "method" dan "song" dari objek JSON menggunakan `json_object_object_get_ex()`. Jika pengambilan nilai berhasil, maka nilai tersebut disimpan dalam variabel `method` dan `song` sebagai string C. Terkait dengan nilai "method" yang diperoleh, dipanggil fungsi yang sesuai untuk mendekripsi data lagu.
    - Jika nilai "method" adalah "rot13", dipanggil fungsi `rot13(playlist, song)` untuk melakukan dekripsi menggunakan metode ROT13.
    - Jika nilai "method" adalah "hex", dipanggil fungsi `hex(playlist, song)` untuk melakukan dekripsi menggunakan metode Hexadecimal.
    - Jika nilai "method" adalah "base64", dilakukan alokasi memori untuk data hasil dekripsi (`outputData`), kemudian dilakukan dekripsi menggunakan fungsi `base64()` dengan mengambil ukuran panjang data lagu (`strlen(song)`) dan menyimpan hasilnya dalam variabel `outputSize`. Data hasil dekripsi ditulis ke file output menggunakan `fwrite()`, dan memori yang dialokasikan untuk `outputData` dihapus menggunakan `free()`.

Setelah melakukan dekripsi dan menulis hasilnya ke file, ditulis karakter newline "\n" ke file output menggunakan `fwrite()` untuk memisahkan setiap entri lagu yang telah didekripsi. Setelah selesai looping, kedua file input dan output ditutup menggunakan `fclose()` untuk menyelesaikan proses dekripsi.
```c
void decrypt() {
    // Open input and output files for reading and writing, respectively
    FILE *songPlaylist = fopen("song-playlist.json", "r");
    FILE *playlist = fopen("playlist.txt", "w");

    // If either of the files failed to open, print an error message and return
    if(songPlaylist == NULL) {
        printf("Error: Failed to open song-playlist.json\n");
        return;
    }
    if(playlist == NULL) {
        printf("Error: Failed to open playlist.txt\n");
        return;
    }

    // Parse the JSON object from the input file
    struct json_object *jsonObj = json_object_from_file("song-playlist.json");

    // If the parsing failed, print an error message and close both files before returning
    if(jsonObj == NULL) {
        printf("Error: Failed to parse JSON\n");
        fclose(songPlaylist);
        fclose(playlist);
        return;
    }

    // Declare variables for holding the JSON method and song values, as well as their C string equivalents
    struct json_object *jsonMethod, *jsonSong;
    char *method, *song;

    // Get the length of the JSON array
    int len = json_object_array_length(jsonObj);

    // Loop through each object in the JSON array
    for(int i=0; i<len; i++) {
        
        // Get the JSON object at the current index
        struct json_object *jsonIndex = json_object_array_get_idx(jsonObj, i);

        // Get the "method" and "song" values from the JSON object
        if(json_object_object_get_ex(jsonIndex, "method", &jsonMethod)) {
            method = (char *)json_object_get_string(jsonMethod);
        }
        if(json_object_object_get_ex(jsonIndex, "song", &jsonSong)) {
            song = (char *)json_object_get_string(jsonSong);
        }

        // Depending on the method specified, call the appropriate function to decrypt the song data
        if(strcasecmp(method, "rot13") == 0) {
            rot13(playlist, song);
        }
        if(strcasecmp(method, "hex") == 0) {
            hex(playlist, song);
        }
        if(strcasecmp(method, "base64") == 0) {
            // Allocate memory for the decoded data, decode the data, write it to the output file, and free the memory
            size_t outputSize;
            char *outputData = base64(song, strlen(song), &outputSize);
            fwrite(outputData, 1, outputSize, playlist);
            free(outputData);
        }
        
        // Write a newline character to the output file to separate each decrypted song entry
        fwrite("\n", 1, 1, playlist);
    }

    // Close both input and output files
    fclose(songPlaylist);
    fclose(playlist);

}
```
Fungsi `msgid` akan menghasilkan unique message identifier dan menetapkannya ke variabel msgid. Lalu, diperiksa apakah message queue berhasil dibuat. Jika tidak, akan mencetak pesan error dan keluar dari program dengan status 1. Semaphore diinisialisasi dengan jumlah 2 dan menetapkannya ke variabel `sem`. Semaphore ini digunakan untuk membatasi jumlah pengguna yang dapat menjalankan perintah secara bersamaan.
```c
int main() {
	int msgid = msgidGenerator();

    if(msgid == -1) {
        printf("Error: Failed to create message queue\n");
        exit(1);
    }
    sem_t sem;
    sem_init(&sem, 0, 2);
```
Variabel struct user bernama userData diinisialisasi dan akan menyimpan perintah pengguna. Lalu, dilakukan loop while yang akan bekerja sampai program terhenti. Pesan dari message queue yang ditentukan oleh msgid akan diterima dan disimpan dalam struct userData. Jika terjadi kesalahan saat menerima pesan, cetak pesan error dan keluar dari program. Nomor pengguna diatur menjadi 1 (asumsikan bahwa pengguna adalah pengguna pertama). Selanjutnya jika diperlukan, update user ID.
```c
    struct user userData;

    while(1) {
        if(msgrcv(msgid, &userData, sizeof(userData.command), 0, 0) == -1){ 
            perror("Failed at receiving message\n");
            exit(1);
        }
        usrnum = 1;
        sem_wait(&sem);

        if(user1Id != 1 && user2Id == 2 && user1Id != userData.id){
            user2Id = userData.id;
        }

        if(user1Id == 1 && user2Id != userData.id) {
            user1Id = userData.id;
            if(user2Id == 1) {
                user2Id++;
            }
        }
```
Potongan code untuk memproses command "exit".
```c

        if(strcasecmp(userData.command, "exit\n") == 0){
            if(user1Id == userData.id) {
                user1Id = 1;
                usrnum = 0;
            }
            else if(user2Id == userData.id) {
                user2Id = 2;
                usrnum = 0;
            }
        }
```
Potongan code untu mengecek apakah stream system overloaded atau tidak.
```c
        if(user1Id != userData.id  && user2Id != userData.id && user1Id != 1 && user2Id > 2) {
            printf("STREAM SYSTEM OVERLOAD\n");
            usrnum = 0;
        }
```
Jika hanya terdapat satu user yang terhubung, command dari mereka akan diproses.
```c
        if(usrnum == 1) {
            // Process the "DECRYPT" command
            if(strcasecmp(userData.command, "DECRYPT\n") == 0) {
                decrypt();
                sort();
            }
            // Process the "LIST" command
            else if(strcasecmp(userData.command, "LIST\n") == 0) {
                list();
            }
            // Process the "PLAY" and "ADD" commands
            else if(strstr(userData.command, "PLAY") != NULL || strstr(userData.command, "ADD") != NULL) {
                // Find the string enclosed in double quotes
                char *start = strchr(userData.command, '"');
                if (start != NULL) {
                    start++;
                    char *end = strchr(start, '"');
                    if (end != NULL) {
                        int len = end - start;
                        char output[len + 1];
                        strncpy(output, start, len);
                        output[len] = '\0';

                        // Process the "PLAY" command
                        if(strstr(userData.command, "PLAY") != NULL) {
                            play(output, userData.id);
                        }
                        // Process the "ADD" command
                        if(strstr(userData.command, "ADD") != NULL) {
                            add(output, userData.id);
                        }
                    }
                }
            }
            // Process any unknown command
            else {
                printf("UNKNOWN COMMAND\n");
            }
        }
        sem_post(&sem);
    }
    sem_destroy(&sem);

	return 0;
}
```
Sementara user.c berisi program yang menggunakan message queue untuk menerima perintah dari pengguna dan mengirimnya ke message queue.

Kendala:
- Pada bagian ADD masih terdapat kesalahan. Seharusnya jika lagu yang di-add ke playlist sudah ada sebelumnya di dalam playlist, outputnya adalah "LAGU SUDAH MASUK PLAYLIST". Kami masih gagal mengimplementasikannya dalam code dan lagu tetap masuk meskipun sebelumnya sudah ada dalam playlist.

Hasil command DECRYPT:

![WhatsApp_Image_2023-05-13_at_4.30.17_PM](/uploads/15fab5032e39c041fe3750b6802abbba/WhatsApp_Image_2023-05-13_at_4.30.17_PM.jpeg)

![WhatsApp_Image_2023-05-13_at_4.30.16_PM](/uploads/e07cb05ac08ec7767773b3d203d4f40f/WhatsApp_Image_2023-05-13_at_4.30.16_PM.jpeg)

Hasil command LIST:

![WhatsApp_Image_2023-05-13_at_4.30.44_PM](/uploads/97a8f6efb62520471bf44539e6890113/WhatsApp_Image_2023-05-13_at_4.30.44_PM.jpeg)

Hasil command PLAY <song>:

![WhatsApp_Image_2023-05-13_at_4.31.02_PM](/uploads/c9e4ac8cdf843bc33adc9c99bcddc577/WhatsApp_Image_2023-05-13_at_4.31.02_PM.jpeg)

Hasil command ADD <song>:

![WhatsApp_Image_2023-05-13_at_7.15.07_PM](/uploads/ee45d5c63b8d25c2d7945a0af12160da/WhatsApp_Image_2023-05-13_at_7.15.07_PM.jpeg)

Hasil pembatasan jumlah user dengan semaphore:

![WhatsApp_Image_2023-05-13_at_4.31.58_PM](/uploads/ffb0fc4f89217698384a1f0f4a635f12/WhatsApp_Image_2023-05-13_at_4.31.58_PM.jpeg)

Hasil command tidak diketahui:

![WhatsApp_Image_2023-05-13_at_4.32.33_PM](/uploads/07f94a32b24dbcd40005f4689c1a5f44/WhatsApp_Image_2023-05-13_at_4.32.33_PM.jpeg)

## Soal 4
Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut! 

a. Download dan unzip file tersebut dalam kode c bernama unzip.c.

b. Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.

c. Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.

    extension_a : banyak_file
    extension_b : banyak_file
    extension_c : banyak_file
    other : banyak_file

d. Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.
e. Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.

    DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
    DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
    DD-MM-YYYY HH:MM:SS MADE [folder name]
    examples : 
    02-05-2023 10:01:02 ACCESSED files
    02-05-2023 10:01:03 ACCESSED files/abcd
    02-05-2023 10:01:04 MADE categorized
    02-05-2023 10:01:05 MADE categorized/jpg
    02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg
Catatan:
- Path dimulai dari folder files atau categorized
- Simpan di dalam log.txt
- ACCESSED merupakan folder files beserta dalamnya
- Urutan log tidak harus sama

f. Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.
- Untuk menghitung banyaknya ACCESSED yang dilakukan.
- Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
- Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.

### Penyelesaian

Pada poin a, diminta untuk melakukan download dan unzip file hehe.zip dengan kode pada file unzip.c. Berikut adalah isi dari unzip.c:

```c
int main(){
    pid_t child_id = fork();
    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"wget", "-q", "-O", "hehe.zip", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL};
        execv("/bin/wget", argv);
    }
    else {
        int status;
        while(wait(&status) > 0);
        char *argv[] = {"unzip", "-q" , "hehe.zip", NULL};
        execv("/bin/unzip", argv);
    }
}

```
Dengan menggunakan `fork()`, child process akan melakukan download file sedangkan parent process akan melakukan `wait()` dan melakukan unzip.

Berikut adalah hasil dari download dan unzip file hehe.zip:
![Screenshot_2023-05-11_124931](/uploads/ce1b013edfb352d761c74aeb39cfa039/Screenshot_2023-05-11_124931.png)

Selanjutnya pada poin b hingga e, diminta untuk membuat program categorize.c yang dapat melakukan beberapa hal berikut:
- Membuat folder categorized.
- Membuat beberapa folder di dalam folder categorized dengan nama-nama extension yang didapat dari extensions.txt dan maksimal isi file sesuai angka yang didapat dari max.txt.
- Melakukan move/copy file-file dari folder files ke folder-folder extension sesuai dengan extensionnya.
- Menggunakan multithread untuk mengakses tiap folder.
- Mengoutputkan pada console banyak file dari tiap extension terurut ascending.
- Membuat log ACCESSED, MADE, dan MOVED pada log.txt untuk setiap folder yang diakses, folder yang dibuat, dan file yang dipindah.

Berikut adalah bagian main dari program categorize.c:
```c
int main(){
    pthread_t tid;
    pthread_create(&tid, NULL, create_categorized, NULL);
    pthread_join(tid, NULL);
    pthread_create(&tid, NULL, read_extensions, NULL);
    pthread_join(tid, NULL);
    pthread_create(&tid, NULL, read_limit, NULL);
    pthread_join(tid, NULL);
    pthread_create(&tid, NULL, visit_directory, "files");
    pthread_join(tid, NULL);
    pthread_create(&tid, NULL, count_categorized, NULL);
    pthread_join(tid, NULL);
    return 0;
}

```
Pada bagian ini akan dipanggil beberapa fungsi dengan menggunakan thread, yaitu:
- `create_categorized()` untuk membuat folder categorized.
- `read_extensions()` untuk membaca extension pada file extensions.txt dan membuat folder-folder dengan nama extension tersebut.
- `read_limit()` untuk membaca maksimal file pada tiap folder extension dari file max.txt.
- `visit_directory()` untuk menelusuri seluruh isi folder files dan melakukan move.
- `count_categorized()` untuk melakukan sorting banyak file tiap extension dan mengoutputkan hasilnya di console.

Berikut adalah isi dari fungsi `create_categorized()`:
```c
void *create_categorized(){
    system("mkdir categorized");
    log_made("categorized");
}
```

Pada fungsi tersebut, dilakukan pembuatan folder categorized dengan command mkdir serta memanggil fungsi `log_made()` untuk membuat log MADE pada log.txt yang berisi sebagai berikut:

```c
void log_made(char *arg){
    time_t t = time(NULL);
    struct tm *curr = localtime(&t);

    char log[1000];
    sprintf(log, "%02d-%02d-%d %02d:%02d:%02d MADE %s", curr->tm_mday, curr->tm_mon + 1, curr->tm_year + 1900, curr->tm_hour, curr->tm_min, curr->tm_sec, arg);

    char command[2000];
    sprintf(command, "echo '%s' >> log.txt", log);

    system(command);
}
```

Selanjutnya, berikut adalah isi dari fungsi `read_extensions()`:

```c
void *read_extensions(){
    FILE *fp = fopen("extensions.txt", "r");
    char temp[100], path[1000];
    while (fgets(temp, sizeof(temp), fp)) {
        strncpy(ex[idx], temp, strlen(temp) - 2);
        sprintf(path, "categorized/%s", ex[idx]);
        create_folder(path);
        sum[idx] = 0;
        idx++;
    }

    fclose(fp);
}
```

Menggunakan file pointer, string `temp` akan mengambil setiap baris dari file extensions.txt, kemudian melakukan copy sebagian string kepada array `ex` (array of char yang berisi nama-nama extension). Kemudian memanggil fungsi `create_folder()` untuk membuat folder extension di dalam folder categorized. Selain itu, dilakukan insialisasi array `sum` (array untuk menyimpan banyaknya file tiap extension).

Fungsi `create_folder()` yang dipanggil berisi sebagai berikut:
```c
void create_folder(char *dest){
    char command[2000];
    sprintf(command, "mkdir '%s'", dest);
    system(command);
    log_made(dest);
}
```
Fungsi tersebut, kurang lebih sama dengan fungsi `create_categorized()` yaitu menggunakan command mkdir dan memanggil fungsi `log_made()`.

Berikutnya fungsi `read_limit()` yang berisi sebagai berikut:
```c
void *read_limit(){
    FILE *fp = fopen("max.txt", "r");
    fscanf(fp, "%d", &mx);
    fclose(fp);
}
```
Pada fungsi tersebut, angka pada file max.txt dibaca dan dimasukkan kedalam variabel global `mx`.

Kemudian berikut adalah isi dari fungsi `visit_directory()`:
```c
void *visit_directory(void *arg){
    log_accessed((char*) arg);

    DIR *dir = opendir((char*) arg);
    struct dirent *entry;
    struct stat sb;

    while (entry = readdir(dir)){
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

        char path[1000];
        sprintf(path, "%s/%s", (char*) arg, entry->d_name);
        if (stat(path, &sb) == -1) continue;

        if (S_ISDIR(sb.st_mode)){
            pthread_t tid;
            pthread_create(&tid, NULL, visit_directory, (void*) path);
            pthread_join(tid, NULL);
        }
        else if (S_ISREG(sb.st_mode)){
            char *ext = strrchr(path, '.');
            if (ext == NULL) ext = strrchr(path, '/');
            ext++;

            char low[100];
            memset(low, '\0', sizeof(low));
            for (int i = 0; i < strlen(ext); i++){
                low[i] = ext[i];
                if (low[i] >= 65 && low[i] <= 90) low[i] += 32;
            }

            bool found = false;
            char dest[1000];
            for (int i = 0; i < idx; i++){
                if (!strcmp(ex[i], low)){
                    found = true;
                    int count = folder_num(low);
                    if (count > 1) sprintf(dest, "categorized/%s (%d)", low, count);
                    else sprintf(dest, "categorized/%s", low);
                    sum[i]++;
                    break;
                }
            }
            if (!found) sprintf(dest, "categorized/other"), other++;

            move_file(path, dest, ext);
        }
    }

    closedir(dir);
}
```

Sederhananya, fungsi ini dibuat untuk mengecek isi dari folder files yang berupa folder maupun file. Jika ditemukan folder, maka akan dilakukan rekursi dengan membuat thread baru yang memanggil fungsi `visit_directory()` dengan argumen berupa path folder tersebut (pada tahap ini lah diterapkan multithreading). Sementara itu, jika ditemukan file, maka extensionnya akan di compare dengan array `ex` 
(array of char yang berisi nama-nama extension), kemudian file akan di move sesuai extensionnya.

Lebih detailnya, setiap entry pada suatu folder yang bukan berupa . dan .. akan di-append dengan path dari argumen fungsi dan dimasukkan ke variabel `path`. kemudian menggunakan fungsi `S_ISDIR()` dan `S_ISREG()` akan dicek apakah isi variabel path berupa directory atau file. Jika berisi file, maka digunakan fungsi `strrchr()` untuk mengambil extension dan disimpan pada variabel `ext`. Karena ketentuan pada soal yang meminta untuk juga memasukkan extension dengan huruf uppercase (file TXT dimasukkan sebagai txt), maka dibuat variabel `low` untuk menyimpan isi variabel `ext` dengan huruf lowercase. Selanjutnya, melakukan compare dengan `strcmp()`. Jika tidak ada extension yang sama, maka file dimasukkan ke folder other. Jika ada extension yang sama, maka dipanggil fungsi `folder_num()` untuk menentukan file akan dipindah ke folder yang mana. Berikut adalah isi dari fungsi `folder_num()`:

```c
int folder_num(char *ext){
    char catz[] = "categorized";
    DIR *dir = opendir(catz);
    struct dirent *entry;
    struct stat sb;

    int res = 1;

    while (1){
        char path[1000];
        if (res == 1) sprintf(path, "%s/%s", catz, ext);
        else sprintf(path, "%s/%s (%d)", catz, ext, res);
        if (!stat(path, &sb)){
            if (count_files(path) < 10){
                closedir(dir);
                return res;
            }
            else res++;
        }
        else {
            closedir(dir);
            return res;
        }
    }
}
```
Fungsi tersebut mengecek folder dengan nama extension dari folder pertama, kedua, ketiga, dst. Dengan memanggil fungsi `count_files()` maka akan dihitung banyaknya file yang sudah dimove ke folder tersebut. Jika folder sudah berisi file sebanyak limit yang ditentukan, maka akan dicek folder selanjutnya. Dan jika folder tersebut masih belum mencapai limit atau folder tersebut belum terbentuk, maka melakukan return urutan folder. Berikut isi dari fungsi `count_files()`:
```c
int count_files(char *fol){
    DIR *dir = opendir(fol);
    struct dirent *entry;
    struct stat sb;

    int res = 0;

    while (entry = readdir(dir)){
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

        char path[1000];
        sprintf(path, "%s/%s", fol, entry->d_name);
        if (stat(path, &sb) == -1) continue;
        res++;
    }

    closedir(dir);
    return res;
}
```

Kembali ke fungsi `visit_directory()`, setelah dilakukan compare maka dilakukan penambahan pada banyaknya file pada extension tersebut, yaitu dengan menginkremen variabel `sum` atau `other`. Kemudian dipanggil fungsi `move_file()` untuk melakukan pemindahan file. Berikut adalah isi fungsinya:

```c
void move_file(char *path, char *dest, char *ext){
    struct stat sb;
    int res = stat(dest, &sb);
    if (res) create_folder(dest);
    
    char command[2000];
    sprintf(command, "mv '%s' '%s'", path, dest);
    system(command);
    log_moved(path, dest, ext);
}

```
File akan dipindah dengan command `mv` dan setelah itu dipanggil fungsi `log_moved()` untuk membuat log MOVED pada log.txt. Berikut adalah isinya:

```c
void log_moved(char *path, char *dest, char *ext){
    time_t t = time(NULL);
    struct tm *curr = localtime(&t);

    char log[1000];
    sprintf(log, "%02d-%02d-%d %02d:%02d:%02d MOVED %s file : %s > %s", curr->tm_mday, curr->tm_mon + 1, curr->tm_year + 1900, curr->tm_hour, curr->tm_min, curr->tm_sec, ext, path, dest);

    char command[2000];
    sprintf(command, "echo '%s' >> log.txt", log);

    sys
``` 

Selain itu, karena fungsi `visit_directory()` ini digunakan untuk mengakses tiap folder, maka dipanggil juga fungsi `log_accessed()` untuk membuat log ACCESSED pada log.txt, berikut adalah isinya:
```c
void log_accessed(char *arg){
    time_t t = time(NULL);
    struct tm *curr = localtime(&t);

    char log[1000];
    sprintf(log, "%02d-%02d-%d %02d:%02d:%02d ACCESSED %s", curr->tm_mday, curr->tm_mon + 1, curr->tm_year + 1900, curr->tm_hour, curr->tm_min, curr->tm_sec, arg);

    char command[2000];
    sprintf(command, "echo '%s' >> log.txt", log);

    system(command);
}
```
Terakhir adalah fungsi `count_categorized()` yang melakukan merge sort dan output ke console, berikut adalah isinya:

```c
void *count_categorized(){
    sort(0, idx - 1);
    for (int i = 0; i < idx; i++) printf("%s : %d\n", ex[i], sum[i]);
    printf("other : %d\n", other);
}
```

Berikut adalah beberapa hasil ketika categorize.c dicompile dan dirun:

![Screenshot_2023-05-11_143349](/uploads/38b473ebde816ff36f3782790539a96a/Screenshot_2023-05-11_143349.png)

![Screenshot_2023-05-11_150709](/uploads/b97aec54013a75c5825dbdb4050c8dcb/Screenshot_2023-05-11_150709.png)

![Screenshot_2023-05-11_143423](/uploads/91dd4df3566c6a8a641604ca49cd1073/Screenshot_2023-05-11_143423.png)

![Screenshot_2023-05-11_143441](/uploads/5ee818f99bba20068e704e2ebcd04158/Screenshot_2023-05-11_143441.png)

![Screenshot_2023-05-11_143511](/uploads/2e24a15b7a38af7972df5eef63229579/Screenshot_2023-05-11_143511.png)

![Screenshot_2023-05-11_143456](/uploads/4f99aa7a714105375563e7729551455e/Screenshot_2023-05-11_143456.png)

Pada poin f, diminta untuk membuat program logchecker.c untuk mengambil informasi-informasi berikut dari log.txt:
- Banyak ACCESSED yang dilakukan.
- List seluruh folder yang dibuat berserta banyak file didalamnya.
- Banyak file tiap extension terurut ascending.

Pada logchecker.c, dibuat struct untuk menyimpan nama dan jumlah. Kemudian dilakukan insialisasi array dari struct tersebut untuk folder dan juga extension, berikut programnya:

```c
struct data {
    char name[10];
    int count;
};

struct data folder[100], ext[100];
```

Selanjutnya, menggunakan file pointer akan dibaca tiap baris dari log.txt sebagai berikut:

```c
FILE *fp = fopen("log.txt", "r");
    char line[1000];
    while (fgets(line, sizeof(line), fp)) {
        if (strstr(line, "ACCESSED") != NULL) acc++;
        else if (strstr(line, "MADE") != NULL){
            char *temp;
            if ((temp = strstr(line, "categorized/")) != NULL){
                temp += strlen("categorized/");
                memset(folder[f].name, '\0', sizeof(folder[f].name));
                strncpy(folder[f].name, temp, strlen(temp) - 1);
                folder[f].count = 0;
                f++;

                if (strstr(temp, "(") == NULL){
                    strcpy(ext[e].name, folder[f-1].name);
                    ext[e].count = 0;
                    e++;
                }
            }
        }
        else if (strstr(line, "MOVED") != NULL){
            char *temp;
            if ((temp = strstr(line, "categorized/")) != NULL){
                temp += strlen("categorized/");
                temp[strlen(temp) - 1] = '\0';

                for (int i = 0; i < f; i++){
                    if (!strcmp(temp, folder[i].name)){
                        folder[i].count++;
                        break;
                    }
                }

                for (int i = 0; i < e; i++){
                    if (strstr(temp, ext[i].name) != NULL){
                        ext[i].count++;
                        break;
                    }
                }
            }
        }
    }
    fclose(fp);
```

Jika pada suatu baris berisi kata ACCESSED, maka banyaknya ACCESSED akan diinkremen. Jika pada suatu baris ditemukan MADE, maka folder yang dibuat akan disimpan ke array `folder`, dan jika nama folder tidak berisi angka maka disimpan juga ke array `ext`. Jika pada suatu baris berisi kata MOVED, maka dilakukan compare nama folder destinasi dan array `folder` dan `ext`, jika compare cocok, maka banyak pada array akan diinkremen.

Sebelum dioutput ke console, dilakukan sort terlebih dahulu pada array `folder` dan `ext` denga merge sort:
```c
sort(folder, 0, f - 1);
sort(ext, 0, e - 1);
```

Berikut adalah output dari logchecker.c:
![Screenshot_2023-05-11_150300](/uploads/ed3fe786248d40adbae25a6499696a81/Screenshot_2023-05-11_150300.png)
