#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <limits.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>

struct user {
	int id;
	char command[1024];
};

int msgidGenerator() {
    key_t key;
    if((key = ftok("user", 65)) == -1) {
        perror("ftok");
        return -1;
    }

    int msgid;
    if((msgid = msgget(key, 0666 | IPC_CREAT)) == -1) {
        perror("msgid");
        return -1;
    }

    return msgid;
}

int main() {
	int msgid = msgidGenerator();
    if(msgid < 0) {
        printf("message queue failed\n");
        exit(1);
    }
    
    while(1) {
        struct user userData;
        userData.id = getpid();

        printf("Write command : ");
    	fgets(userData.command, 1024, stdin);

        if(strcasecmp(userData.command, "exit\n") == 0)
            break;

        if(msgsnd(msgid, &userData, sizeof(userData.command), 0) == -1) {
            perror("msgsnd");
            exit(1);
        };
    }

	return 0;
}

