#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>
#include <limits.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <json-c/json.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <ctype.h>
#include <semaphore.h>

int user1Id = 1, user2Id = 1, usrnum = 1;

struct user {
	int id;
	char command[1024];
};

// The function msgidGenerator() generates a unique message queue identifier
// using the ftok() and msgget() system calls. The identifier can be used to
// create or access a message queue.
int msgidGenerator() {
    key_t key;
    
    // Generate a unique key for the message queue using ftok().
    // The first argument is a filename (in this case "user") that is used to
    // generate the key. The second argument is a unique identifier for the key.
    // If the ftok() function fails, it returns -1 and sets errno to indicate the error.
    if((key = ftok("user", 65)) == -1) {
        perror("ftok");
        return -1;
    }

    int msgid;
    
    // Create or access a message queue using the generated key and msgget().
    // The first argument is the key generated using ftok(). The second argument
    // is a set of flags that control the creation and access of the message queue.
    // In this case, the flags specify that the message queue should be created if it
    // does not already exist, and that it should be readable and writable by all users.
    // If the msgget() function fails, it returns -1 and sets errno to indicate the error.
    if((msgid = msgget(key, 0666 | IPC_CREAT)) == -1) {
        perror("msgid");
        return -1;
    }

    // Return the message queue identifier.
    return msgid;
}

// Define a function called "add" that takes a pointer to a character array and an integer as arguments
void add(char* word, int id) {    

    // Declare a file pointer and open the "playlist.txt" file in read mode
    FILE *fp = fopen("playlist.txt", "r");    

    // Declare a character array to hold each line of the file
    char line[100];    

     // Use a loop to read each line of the file
    while (fgets(line, 100, fp) != NULL) {   

        // Check if the current line is the same as the word to be added
        if (strcmp(line, word) == 0) {    

            // If it is, print a message indicating that the song is already on the playlist
            printf("SONG ALREADY ON PLAYLIST\n");    

            // Close the file
            fclose(fp);    

            // Return from the function without doing anything else
            return;    
        }
    }

    // Close the file
    fclose(fp);    

    // Re-open the file in append mode
    fp = fopen("playlist.txt", "a");    

    // Add the word to the file
    fprintf(fp, "%s\n", word);    
    
    // Close the file
    fclose(fp);    

    // Print a message indicating that the specified user has added the specified word to the playlist
    printf("USER %d ADD %s\n", id, word);    
}

void play(char* word, int id) {
    FILE* file = fopen("playlist.txt", "r"); // Open the playlist file
    if (!file) {
        printf("Failed to open playlist.txt\n"); // If the file can't be opened, print an error message and return
        return;
    }

    char line[1000];
    char lst[1000][1000]; // Create an array to store matching songs
    int found_count = 0; // Counter for the number of matches found

    char search_word[1000];
    int i;
    for (i = 0; word[i]; i++) { // Convert the search term to lowercase
        search_word[i] = tolower(word[i]);
    }
    search_word[i] = '\0';

    while (fgets(line, 1000, file)) { // Read the file line by line
        int line_length = strlen(line);

        char lower_line[1000];
        int j;
        for (j = 0; j < line_length; j++) { // Convert the line to lowercase
            lower_line[j] = tolower(line[j]);
        }
        lower_line[j] = '\0';

        if (strstr(lower_line, search_word) != NULL) { // Check if the search term is in the line
            sprintf(lst[found_count], "%s", line); // Add the line to the list of matches
            found_count++; // Increment the counter
        }
    }

    if(found_count == 1) { // If there's only one match, play the song
        printf("USER <%d> PLAYING \"%s\"\n", id, lst[0]);
    }
    else if (found_count == 0) { // If there are no matches, print an error message
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", word);
    }
    else { // If there are multiple matches, print a list of them
        printf("THERE ARE %d SONG CONTAINING \"%s\"\n", found_count, word);
        for (int i=0; i<found_count; i++) {
            printf("%d. %s", i+1, lst[i]);
        }
    }

    fclose(file); // Close the file
}

void list() {
    // Open file "playlist.txt" for reading
    FILE *filePointer;
    filePointer = fopen("playlist.txt", "r");
    // Check if the file was opened successfully
    if (filePointer == NULL) {
        printf("File cannot be opened.\n");
        return;
   }

   char ch;
   // Read characters from file until EOF is reached
   while ((ch = fgetc(filePointer)) != EOF)
      printf("%c", ch); // Print each character

   fclose(filePointer); // Close the file
}

void rot13(FILE *file, char *str) {
    // Loop through each character in the string
    for (int i=0; str[i]!='\0'; i++) {
        if ((str[i] >= 'a' && str[i] <= 'm') || (str[i] >= 'A' && str[i] <= 'M')) {
            fputc((str[i] + 13), file); // If the character is in range A-M or a-m, add 13 to its ASCII value and write to file
        } else if ((str[i] >= 'n' && str[i] <= 'z') || (str[i] >= 'N' && str[i] <= 'Z')) {
            fputc((str[i] - 13), file); // If the character is in range N-Z or n-z, subtract 13 from its ASCII value and write to file
        }
        else {
            fputc(str[i], file); // If the character is not a letter, write it to file without any modification
        }
    }
}

char *base64(char *input, size_t input_len, size_t *output_len) {
    // Declare variables
    BIO *bio, *b64;
    char *output = (char*)malloc(input_len); // Allocate memory for output
    memset(output, 0, input_len); // Initialize output to zero
    
    // Create BIO objects
    bio = BIO_new_mem_buf(input, input_len); 
    b64 = BIO_new(BIO_f_base64()); 
    bio = BIO_push(b64, bio); 
    
    // Set flags for BIO
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

    // Read from BIO and store in output
    *output_len = BIO_read(bio, output, input_len); 
    
    // Free memory and return output
    BIO_free_all(bio); 
    return output;
}

void hex(FILE *file, char *str) {
    while (*str) {
        int value = 0;
        for (int i = 0; i < 2 && *str; i++) { // Read two characters at a time
            value <<= 4; // Shift previous value left by 4 bits
            if (*str >= '0' && *str <= '9') { // Convert from ASCII to integer
                value |= (*str - '0');
            } else if (*str >= 'a' && *str <= 'f') {
                value |= (*str - 'a' + 10);
            } else if (*str >= 'A' && *str <= 'F') {
                value |= (*str - 'A' + 10);
            } else {
                fprintf(stderr, "Error: Invalid hexadecimal digit '%c'\n", *str);
                return; // Stop processing if invalid digit is encountered
            }
            str++;
        }
        fputc(value, file); // Write value to file
    }
}

// This function takes two pointers to char pointers and swaps the values they point to.
void swap(char **a, char **b) {
    char *temp = *a;
    *a = *b;
    *b = temp;
}

// This function takes an array of char pointers, an integer left, and an integer right. 
// It uses the right-most element of the array as the pivot, and moves all elements 
// smaller than the pivot to its left and all elements greater than the pivot to its 
// right. It then swaps the pivot with the first element greater than it, and returns 
// the index of the pivot.
int partition(char **lines, int left, int right) {
    char *pivot = lines[right];
    int i = left - 1;
    for (int j = left; j < right; j++) {
        if (strcasecmp(lines[j], pivot) < 0) {
            i++;
            swap(&lines[i], &lines[j]);
        }
    }
    swap(&lines[i + 1], &lines[right]);
    return i + 1;
}

// This function takes an array of char pointers, an integer left, and an integer right. 
// It checks if left is less than right, and if so, partitions the array using the 
// partition function and recursively calls itself on the left and right partitions.
void quicksort(char **lines, int left, int right) {
    if (left < right) {
        int pivot_idx = partition(lines, left, right);
        quicksort(lines, left, pivot_idx - 1);
        quicksort(lines, pivot_idx + 1, right);
    }
}

// This function reads the lines of the file "playlist.txt" into an array of char pointers, 
// then sorts the array using quicksort. It then overwrites the contents of the file with 
// the sorted lines and frees the memory used by the array of char pointers. Finally, it 
// prints a message indicating that the file has been sorted.
void sort() {
    int MAX_LINE_LEN = 10000;
    FILE *fp = fopen("playlist.txt", "r");
    if (!fp) {
        printf("Error: cannot open file playlist.txt\n");
        return ;
    }

    char **lines = malloc(sizeof(char*) * MAX_LINE_LEN);
    int num_lines = 0;
    char line[MAX_LINE_LEN];
    while (fgets(line, MAX_LINE_LEN, fp) != NULL) {
        lines[num_lines] = strdup(line);
        num_lines++;
    }
    fclose(fp);

    quicksort(lines, 0, num_lines - 1);

    fp = fopen("playlist.txt", "w");
    for (int i = 0; i < num_lines; i++) {
        fprintf(fp, "%s", lines[i]);
        free(lines[i]);
    }
    fclose(fp);
    free(lines);

    printf("File has been sorted.\n");

}

void decrypt() {
    // Open input and output files for reading and writing, respectively
    FILE *songPlaylist = fopen("song-playlist.json", "r");
    FILE *playlist = fopen("playlist.txt", "w");

    // If either of the files failed to open, print an error message and return
    if(songPlaylist == NULL) {
        printf("Error: Failed to open song-playlist.json\n");
        return;
    }
    if(playlist == NULL) {
        printf("Error: Failed to open playlist.txt\n");
        return;
    }

    // Parse the JSON object from the input file
    struct json_object *jsonObj = json_object_from_file("song-playlist.json");

    // If the parsing failed, print an error message and close both files before returning
    if(jsonObj == NULL) {
        printf("Error: Failed to parse JSON\n");
        fclose(songPlaylist);
        fclose(playlist);
        return;
    }

    // Declare variables for holding the JSON method and song values, as well as their C string equivalents
    struct json_object *jsonMethod, *jsonSong;
    char *method, *song;

    // Get the length of the JSON array
    int len = json_object_array_length(jsonObj);

    // Loop through each object in the JSON array
    for(int i=0; i<len; i++) {
        
        // Get the JSON object at the current index
        struct json_object *jsonIndex = json_object_array_get_idx(jsonObj, i);

        // Get the "method" and "song" values from the JSON object
        if(json_object_object_get_ex(jsonIndex, "method", &jsonMethod)) {
            method = (char *)json_object_get_string(jsonMethod);
        }
        if(json_object_object_get_ex(jsonIndex, "song", &jsonSong)) {
            song = (char *)json_object_get_string(jsonSong);
        }

        // Depending on the method specified, call the appropriate function to decrypt the song data
        if(strcasecmp(method, "rot13") == 0) {
            rot13(playlist, song);
        }
        if(strcasecmp(method, "hex") == 0) {
            hex(playlist, song);
        }
        if(strcasecmp(method, "base64") == 0) {
            // Allocate memory for the decoded data, decode the data, write it to the output file, and free the memory
            size_t outputSize;
            char *outputData = base64(song, strlen(song), &outputSize);
            fwrite(outputData, 1, outputSize, playlist);
            free(outputData);
        }
        
        // Write a newline character to the output file to separate each decrypted song entry
        fwrite("\n", 1, 1, playlist);
    }

    // Close both input and output files
    fclose(songPlaylist);
    fclose(playlist);

}

int main() {

    // Generates a unique message queue identifier and assigns it to the variable msgid.
	int msgid = msgidGenerator();

    // Checks if the message queue was successfully created. If not, prints an error 
    // message and exits the program with a status of 1.
    if(msgid == -1) {
        printf("Error: Failed to create message queue\n");
        exit(1);
    }

    // Initializes a semaphore with a count of 2 and assigns it to the variable sem. This 
    // semaphore is used to limit the number of users that can execute commands at the 
    // same time.
    sem_t sem;
    sem_init(&sem, 0, 2);

    //Defines a struct user variable called userData that will hold the user's command.
    struct user userData;

    // Starts an infinite loop that will run until the program is terminated.
    while(1) {

        // Receives a message from the message queue specified by msgid and stores it in 
        // the userData struct. If there is an error receiving the message, it prints an 
        // error message and exits the program with a status of 1.
        if(msgrcv(msgid, &userData, sizeof(userData.command), 0, 0) == -1){ 
            perror("Failed at receiving message\n");
            exit(1);
        }// Set the user number to 1 (assume that the user is the first user)
        usrnum = 1;
        // Wait on the semaphore to ensure mutual exclusion
        sem_wait(&sem);

        // Update the user IDs if necessary
        if(user1Id != 1 && user2Id == 2 && user1Id != userData.id){
            user2Id = userData.id;
        }

        if(user1Id == 1 && user2Id != userData.id) {
            user1Id = userData.id;
            if(user2Id == 1) {
                user2Id++;
            }
        }

        // Process the "exit" command
        if(strcasecmp(userData.command, "exit\n") == 0){
            if(user1Id == userData.id) {
                user1Id = 1;
                usrnum = 0;
            }
            else if(user2Id == userData.id) {
                user2Id = 2;
                usrnum = 0;
            }
        }

        // Check if the stream system is overloaded
        if(user1Id != userData.id  && user2Id != userData.id && user1Id != 1 && user2Id > 2) {
            printf("STREAM SYSTEM OVERLOAD\n");
            usrnum = 0;
        }

        // If there is only one user connected, process their commands
        if(usrnum == 1) {
            // Process the "DECRYPT" command
            if(strcasecmp(userData.command, "DECRYPT\n") == 0) {
                decrypt();
                sort();
            }
            // Process the "LIST" command
            else if(strcasecmp(userData.command, "LIST\n") == 0) {
                list();
            }
            // Process the "PLAY" and "ADD" commands
            else if(strstr(userData.command, "PLAY") != NULL || strstr(userData.command, "ADD") != NULL) {
                // Find the string enclosed in double quotes
                char *start = strchr(userData.command, '"');
                if (start != NULL) {
                    start++;
                    char *end = strchr(start, '"');
                    if (end != NULL) {
                        int len = end - start;
                        char output[len + 1];
                        strncpy(output, start, len);
                        output[len] = '\0';

                        // Process the "PLAY" command
                        if(strstr(userData.command, "PLAY") != NULL) {
                            play(output, userData.id);
                        }
                        // Process the "ADD" command
                        if(strstr(userData.command, "ADD") != NULL) {
                            add(output, userData.id);
                        }
                    }
                }
            }
            // Process any unknown command
            else {
                printf("UNKNOWN COMMAND\n");
            }
        }

        sem_post(&sem);
    }

    sem_destroy(&sem);

	return 0;
}
 
