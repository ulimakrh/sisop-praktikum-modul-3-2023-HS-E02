#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>

char *mult(char c1[], char c2[]){
    int s1 = strlen(c1) - 1, s2 = strlen(c2) - 1, u = 0, v = 0;
    int temp[300], digits = 0;
    memset(temp, 0, sizeof(temp));

    for (int i = s1; i >= 0; i--){
        int carry = 0, n1 = c1[i] - '0';
        v = 0;
        for (int j = s2; j >= 0; j--){
            int n2 = c2[j] - '0';
            int sum = n1 * n2 + temp[u + v] + carry;
            carry = sum / 10;
            temp[u + v] = sum % 10;
            if (u + v > digits) digits = u + v;
            v++;
        }

        temp[u + v] += carry;
        if (u + v > digits) digits = u + v;
        u++;
    }

    int dig = digits;
    for (int i = dig; i >= 0; i--){
        if (!temp[i]) digits--;
        else break;
    }

    char *res = (char*) malloc((digits + 1) * sizeof(char));
    for (int i = 0; i <= digits; i++) res[i] = temp[digits - i] + '0';
    res[digits + 1] = '\0';
    return res;
}

void hitung_faktorial(int num){
    char res[300], c[300];
    strcpy(res, "1");
    for (int i = num; i >= 1; i--){
        sprintf(c, "%d", i);
        strcpy(res, mult(res, c));
    }

    printf("%s ", res);
}


int main(){
    clock_t start, end;
    start = clock();
    key_t key = 1234;
    int (*hasil)[5];
    
    int shmid = shmget(key, sizeof(int[4][5]), 0666);
    hasil = shmat(shmid, NULL, 0);

    printf("matriks hasil perkalian\n");
    for(int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }

    printf("\nmatriks hasil faktorial\n");
    int index = 0;
    for(int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            hitung_faktorial(hasil[i][j]);
        }
        printf("\n");
    }

    shmdt(hasil);
    end = clock();
    double time = (double)(end - start) / CLOCKS_PER_SEC;;
    printf("\ntime taken: %lf\n", time);
    return 0;
}