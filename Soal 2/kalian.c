#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <time.h>

int main(){
    key_t key = 1234;
    int (*hasil)[5];
    
    int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);
    hasil = shmat(shmid, NULL, 0);

    int m1[4][2], m2[2][5];
    srand(time(NULL));

    for(int i=0; i<4; i++){
        for(int j=0; j<2; j++){
            m1[i][j] = rand() % 5 + 1;
        }
    }

    printf("matriks 1\n");
    for(int i=0; i<4; i++){
        for(int j=0; j<2; j++){
            printf("%d ", m1[i][j]);
        }
        printf("\n");
    }

    for(int i=0; i<2; i++){
        for(int j=0; j<5; j++){
            m2[i][j] = rand() % 4 + 1;
        }
    }

    printf("\nmatriks 2\n");
    for(int i=0; i<2; i++){
        for(int j=0; j<5; j++){
            printf("%d ", m2[i][j]);
        }
        printf("\n");
    }

    for(int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            hasil[i][j] = 0;
            for(int k=0; k<2; k++){
                hasil[i][j] += m1[i][k] * m2[k][j];
            }
        }
    }

    printf("\nmatriks hasil\n");
    for(int i=0; i<4; i++){
        for(int j=0; j<5; j++){
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }

    shmdt(hasil);

    return 0;
}