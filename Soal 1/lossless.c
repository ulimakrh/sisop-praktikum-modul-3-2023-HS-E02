#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

struct MinHeapNode{    
    char letter;
    int count;
    struct MinHeapNode *left, *right;
};

struct MinHeap{
    int size, capacity;
    struct MinHeapNode **array;  
};

struct MinHeapNode *newNode(char letter, int count){
    struct MinHeapNode *temp = (struct MinHeapNode*) malloc(sizeof(struct MinHeapNode));
    temp->left = temp->right = NULL;
    temp->letter = letter;
    temp->count = count;
    return temp;
}

struct MinHeap *createMinHeap(int capacity){
    struct MinHeap *minHeap = (struct MinHeap*) malloc(sizeof(struct MinHeap));
    minHeap->size = 0;
    minHeap->capacity = capacity;
    minHeap->array = (struct MinHeapNode**) malloc(minHeap->capacity * sizeof(struct MinHeapNode*));
    return minHeap;
}

void swapNode(struct MinHeapNode **a, struct MinHeapNode **b){
    struct MinHeapNode *t = *a;
    *a = *b;
    *b = t;
}

void minHeapify(struct MinHeap *minHeap, int i){
    int smallest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < minHeap->size && minHeap->array[left]->count < minHeap->array[smallest]->count) smallest = left;
    if (right < minHeap->size && minHeap->array[right]->count < minHeap->array[smallest]->count) smallest = right;
    if (smallest != i){
        swapNode(&minHeap->array[smallest], &minHeap->array[i]);
        minHeapify(minHeap, smallest);
    }
}

struct MinHeapNode *getMin(struct MinHeap *minHeap){
    struct MinHeapNode *temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];
    minHeap->size--;
    minHeapify(minHeap, 0);
    return temp;
}

void insertMinHeap(struct MinHeap *minHeap, struct MinHeapNode *minHeapNode){
    minHeap->size++;
    int i = minHeap->size - 1;

    while (i && minHeapNode->count < minHeap->array[(i - 1) / 2]->count){
        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }

    minHeap->array[i] = minHeapNode;
}

void buildMinHeap(struct MinHeap *minHeap){
    for (int i = (minHeap->size - 2) / 2; i >= 0; i--) minHeapify(minHeap, i);
}

struct MinHeap *createAndBuildMinHeap(char letter[], int count[], int size){
    struct MinHeap *minHeap = createMinHeap(size);
    for (int i = 0; i < size; i++) minHeap->array[i] = newNode(letter[i], count[i]);
    minHeap->size = size;
    buildMinHeap(minHeap);
    return minHeap;
}

struct MinHeapNode *buildHuffmanTree(char letter[], int count[], int size){
    struct MinHeapNode *left, *right, *top;
    struct MinHeap *minHeap = createAndBuildMinHeap(letter, count, size);

    while(minHeap->size != 1){
        left = getMin(minHeap);
        right = getMin(minHeap);
        top = newNode('$', left->count + right->count);
        top->left = left;
        top->right = right;
        insertMinHeap(minHeap, top);
    }

    return getMin(minHeap);
}

void printCodes(struct MinHeapNode *root, int temp[], char huffman[][26], int top){
    if (root->left){
        temp[top] = 0;
        printCodes(root->left, temp, huffman, top + 1);
    }

    if (root->right){
        temp[top] = 1;
        printCodes(root->right, temp, huffman, top + 1);
    }

    if (!(root->left) && !(root->right)){
        for (int i = 0; i < top; i++){
            if (temp[i]) huffman[root->letter - 'A'][i] = '1';
            else huffman[root->letter - 'A'][i] = '0';
        }
    }
}

void huffmanCodes(char letter[], int count[], char huffman[][26], int size){
    struct MinHeapNode *root = buildHuffmanTree(letter, count, size);
    int temp[26];
    printCodes(root, temp, huffman, 0);
}

void merge(char letter[], int count[], int left, int mid, int right){
    int c1[mid - left + 1], c2[right - mid];
    char l1[mid - left + 1], l2[right - mid];

    for (int i = 0; i < mid - left + 1; i++) c1[i] = count[left + i], l1[i] = letter[left + i];
    for (int i = 0; i < right - mid; i++) c2[i] = count[mid + 1 + i], l2[i] = letter[mid + 1 + i];

    int u = 0, v = 0;
    for (int i = left; i <= right; i++){
        if (u < mid - left + 1 && v < right - mid){
            if (!c1[u] && c2[v]) count[i] = c2[v], letter[i] = l2[v], v++;
            else if (c1[u] && !c2[v]) count[i] = c1[u], letter[i] = l1[u], u++;
            else if (c1[u] >= c2[v]) count[i] = c2[v], letter[i] = l2[v], v++;
            else count[i] = c1[u], letter[i] = l1[u], u++;
        }
        else if (u < mid - left + 1) count[i] = c1[u], letter[i] = l1[u], u++;
        else count[i] = c2[v], letter[i] = l2[v], v++;
    }
}

void sort(char letter[], int count[], int left, int right){
    if (left >= right) return;
    int mid = (left + right) / 2;
    sort(letter, count, left, mid);
    sort(letter, count, mid + 1, right);
    merge(letter, count, left, mid, right);
}

int main(){
    int fd1[2], fd2[2], count[26];
    char letter[26], huffman[26][26];

    for (int i = 0; i < 26; i++) letter[i] = 'A' + i, count[i] = 0;
    
    if (pipe(fd1) == -1 || pipe(fd2) == -1) exit(1);
    
    pid_t child_id = fork();
    if (child_id < 0) exit(1);
    else if (child_id > 0){
        close(fd1[0]);
        close(fd2[1]);

        FILE *fp = fopen("file.txt", "r");
        char c;
        while ((c = fgetc(fp)) != EOF){
            if (c >= 97 && c <= 122) count[c - 'a']++;
            else if (c >= 65 && c <= 90) count[c - 'A']++;
        }
        fclose(fp);

        write(fd1[1], count, sizeof(count));
        close(fd1[1]);

        wait(NULL);
        
        read(fd2[0], huffman, sizeof(huffman));
        printf("Huffman Codes:\n");
        for (int i = 0; i < 26; i++){
            printf("%c %s\n", 'A' + i, huffman[i]);
        }

        int before = 0, after = 0;
        fp = fopen("file.txt", "r");
        while ((c = fgetc(fp)) != EOF){
            if (c >= 97 && c <= 122) before += 8, after += strlen(huffman[c - 'a']);
            else if (c >= 65 && c <= 90) before += 8, after += strlen(huffman[c - 'A']);
        }
        fclose(fp);

        printf("\nASCII: %d bits\n", before);
        printf("Huffman: %d bits\n", after);
        close(fd2[0]);
    }
    else {
        close(fd1[1]);
        close(fd2[0]);
        read(fd1[0], count, sizeof(count));
        close(fd1[0]);

        sort(letter, count, 0, 25);
        int size = 26;
        for (int i = 0; i < 26; i++){
            if (!count[i]) size--;
            for (int j = 0; j < 26; j++) huffman[i][j] = '\0';
        }
        huffmanCodes(letter, count, huffman, size);
        
        write(fd2[1], huffman, sizeof(huffman));
        close(fd2[1]);

        exit(0);
    }
}
