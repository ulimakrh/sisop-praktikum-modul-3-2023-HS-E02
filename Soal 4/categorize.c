#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <dirent.h>
#include <time.h>

char ex[10][100];
int mx, idx = 0, sum[10], other = 0;

void merge(int left, int mid, int right){
    int s1[mid -left + 1], s2[right - mid];
    char e1[mid - left + 1][100], e2[right - mid + 1][100];

    for (int i = 0; i < mid - left + 1; i++) s1[i] = sum[left + i], strcpy(e1[i], ex[left + i]);
    for (int i = 0; i < right - mid; i++) s2[i] = sum[mid + 1 + i], strcpy(e2[i], ex[mid + 1 + i]);

    int u = 0, v = 0;
    for (int i = left; i <= right; i++){
        if (u < mid - left + 1 && v < right - mid){
            if (s1[u] < s2[v]) sum[i] = s1[u], strcpy(ex[i], e1[u]), u++;
            else if (s1[u] == s2[v] && strcmp(e1[u], e2[v]) < 0) sum[i] = s1[u], strcpy(ex[i], e1[u]), u++;
            else sum[i] = s2[v], strcpy(ex[i], e2[v]), v++;
        }
        else if (u < mid - left + 1) sum[i] = s1[u], strcpy(ex[i], e1[u]), u++;
        else sum[i] = s2[v], strcpy(ex[i], e2[v]), v++;
    }
}

void sort(int left, int right){
    if (left >= right) return;
    int mid = (left + right) / 2;
    sort(left, mid);
    sort(mid + 1, right);
    merge(left, mid, right);
}

void log_moved(char *path, char *dest, char *ext){
    time_t t = time(NULL);
    struct tm *curr = localtime(&t);

    char log[1000];
    sprintf(log, "%02d-%02d-%d %02d:%02d:%02d MOVED %s file : %s > %s", curr->tm_mday, curr->tm_mon + 1, curr->tm_year + 1900, curr->tm_hour, curr->tm_min, curr->tm_sec, ext, path, dest);

    char command[2000];
    sprintf(command, "echo '%s' >> log.txt", log);

    system(command);
}

void log_made(char *arg){
    time_t t = time(NULL);
    struct tm *curr = localtime(&t);

    char log[1000];
    sprintf(log, "%02d-%02d-%d %02d:%02d:%02d MADE %s", curr->tm_mday, curr->tm_mon + 1, curr->tm_year + 1900, curr->tm_hour, curr->tm_min, curr->tm_sec, arg);

    char command[2000];
    sprintf(command, "echo '%s' >> log.txt", log);

    system(command);
}

void log_accessed(char *arg){
    time_t t = time(NULL);
    struct tm *curr = localtime(&t);

    char log[1000];
    sprintf(log, "%02d-%02d-%d %02d:%02d:%02d ACCESSED %s", curr->tm_mday, curr->tm_mon + 1, curr->tm_year + 1900, curr->tm_hour, curr->tm_min, curr->tm_sec, arg);

    char command[2000];
    sprintf(command, "echo '%s' >> log.txt", log);

    system(command);
}

void create_folder(char *dest){
    char command[2000];
    sprintf(command, "mkdir '%s'", dest);
    system(command);
    log_made(dest);
}

void move_file(char *path, char *dest, char *ext){
    struct stat sb;
    int res = stat(dest, &sb);
    if (res) create_folder(dest);
    
    char command[2000];
    sprintf(command, "mv '%s' '%s'", path, dest);
    system(command);
    log_moved(path, dest, ext);
}

int count_files(char *fol){
    DIR *dir = opendir(fol);
    struct dirent *entry;
    struct stat sb;

    int res = 0;

    while (entry = readdir(dir)){
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

        char path[1000];
        sprintf(path, "%s/%s", fol, entry->d_name);
        if (stat(path, &sb) == -1) continue;
        res++;
    }

    closedir(dir);
    return res;
}

int folder_num(char *ext){
    char catz[] = "categorized";
    DIR *dir = opendir(catz);
    struct dirent *entry;
    struct stat sb;

    int res = 1;

    while (1){
        char path[1000];
        if (res == 1) sprintf(path, "%s/%s", catz, ext);
        else sprintf(path, "%s/%s (%d)", catz, ext, res);
        if (!stat(path, &sb)){
            if (count_files(path) < 10){
                closedir(dir);
                return res;
            }
            else res++;
        }
        else {
            closedir(dir);
            return res;
        }
    }
}

void *count_categorized(){
    sort(0, idx - 1);
    for (int i = 0; i < idx; i++) printf("%s : %d\n", ex[i], sum[i]);
    printf("other : %d\n", other);
}

void *visit_directory(void *arg){
    log_accessed((char*) arg);

    DIR *dir = opendir((char*) arg);
    struct dirent *entry;
    struct stat sb;

    while (entry = readdir(dir)){
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

        char path[1000];
        sprintf(path, "%s/%s", (char*) arg, entry->d_name);
        if (stat(path, &sb) == -1) continue;

        if (S_ISDIR(sb.st_mode)){
            pthread_t tid;
            pthread_create(&tid, NULL, visit_directory, (void*) path);
            pthread_join(tid, NULL);
        }
        else if (S_ISREG(sb.st_mode)){
            char *ext = strrchr(path, '.');
            if (ext == NULL) ext = strrchr(path, '/');
            ext++;

            char low[100];
            memset(low, '\0', sizeof(low));
            for (int i = 0; i < strlen(ext); i++){
                low[i] = ext[i];
                if (low[i] >= 65 && low[i] <= 90) low[i] += 32;
            }

            bool found = false;
            char dest[1000];
            for (int i = 0; i < idx; i++){
                if (!strcmp(ex[i], low)){
                    found = true;
                    int count = folder_num(low);
                    if (count > 1) sprintf(dest, "categorized/%s (%d)", low, count);
                    else sprintf(dest, "categorized/%s", low);
                    sum[i]++;
                    break;
                }
            }
            if (!found) sprintf(dest, "categorized/other"), other++;

            move_file(path, dest, ext);
        }
    }

    closedir(dir);
}

void *read_limit(){
    FILE *fp = fopen("max.txt", "r");
    fscanf(fp, "%d", &mx);
    fclose(fp);
}

void *read_extensions(){
    FILE *fp = fopen("extensions.txt", "r");
    char temp[100], path[1000];
    while (fgets(temp, sizeof(temp), fp)) {
        strncpy(ex[idx], temp, strlen(temp) - 2);
        sprintf(path, "categorized/%s", ex[idx]);
        create_folder(path);
        sum[idx] = 0;
        idx++;
    }

    fclose(fp);
}

void *create_categorized(){
    system("mkdir categorized");
    log_made("categorized");
}

int main(){
    pthread_t tid;
    pthread_create(&tid, NULL, create_categorized, NULL);
    pthread_join(tid, NULL);
    pthread_create(&tid, NULL, read_extensions, NULL);
    pthread_join(tid, NULL);
    pthread_create(&tid, NULL, read_limit, NULL);
    pthread_join(tid, NULL);
    pthread_create(&tid, NULL, visit_directory, "files");
    pthread_join(tid, NULL);
    pthread_create(&tid, NULL, count_categorized, NULL);
    pthread_join(tid, NULL);
    return 0;
}
