#include <stdio.h>
#include <string.h>

struct data {
    char name[10];
    int count;
};

void merge(struct data s[], int left, int mid, int right){
    struct data s1[mid -left + 1], s2[right - mid];

    for (int i = 0; i < mid - left + 1; i++) s1[i] = s[left + i];
    for (int i = 0; i < right - mid; i++) s2[i] = s[mid + 1 + i];

    int u = 0, v = 0;
    for (int i = left; i <= right; i++){
        if (u < mid - left + 1 && v < right - mid){
            if (s1[u].count < s2[v].count) s[i] = s1[u], u++;
            else if (s1[u].count == s2[v].count && strcmp(s1[u].name, s2[v].name) < 0) s[i] = s1[u], u++;
            else s[i] = s2[v], v++;
        }
        else if (u < mid - left + 1) s[i] = s1[u], u++;
        else s[i] = s2[v], v++;
    }
}

void sort(struct data s[], int left, int right){
    if (left >= right) return;
    int mid = (left + right) / 2;
    sort(s, left, mid);
    sort(s, mid + 1, right);
    merge(s, left, mid, right);
}

int main(){
    struct data folder[100], ext[100];
    int acc = 0, f = 0, e = 0;
    
    FILE *fp = fopen("log.txt", "r");
    char line[1000];
    while (fgets(line, sizeof(line), fp)) {
        if (strstr(line, "ACCESSED") != NULL) acc++;
        else if (strstr(line, "MADE") != NULL){
            char *temp;
            if ((temp = strstr(line, "categorized/")) != NULL){
                temp += strlen("categorized/");
                memset(folder[f].name, '\0', sizeof(folder[f].name));
                strncpy(folder[f].name, temp, strlen(temp) - 1);
                folder[f].count = 0;
                f++;

                if (strstr(temp, "(") == NULL){
                    strcpy(ext[e].name, folder[f-1].name);
                    ext[e].count = 0;
                    e++;
                }
            }
        }
        else if (strstr(line, "MOVED") != NULL){
            char *temp;
            if ((temp = strstr(line, "categorized/")) != NULL){
                temp += strlen("categorized/");
                temp[strlen(temp) - 1] = '\0';

                for (int i = 0; i < f; i++){
                    if (!strcmp(temp, folder[i].name)){
                        folder[i].count++;
                        break;
                    }
                }

                for (int i = 0; i < e; i++){
                    if (strstr(temp, ext[i].name) != NULL){
                        ext[i].count++;
                        break;
                    }
                }
            }
        }
    }
    fclose(fp);

    sort(folder, 0, f - 1);
    sort(ext, 0, e - 1);

    printf("ACCESSED : %d\n", acc);
    printf("\nMADE folders\n");
    for (int i = 0; i < f; i++) printf("%s : %d\n", folder[i].name, folder[i].count);
    printf("\nextensions\n");
    for (int i = 0; i < e; i++) printf("%s : %d\n", ext[i].name, ext[i].count);
}
