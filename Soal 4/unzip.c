#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <wait.h>

int main(){
    pid_t child_id = fork();
    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *argv[] = {"wget", "-q", "-O", "hehe.zip", "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download", NULL};
        execv("/bin/wget", argv);
    }
    else {
        int status;
        while(wait(&status) > 0);
        char *argv[] = {"unzip", "-q" , "hehe.zip", NULL};
        execv("/bin/unzip", argv);
    }
}
